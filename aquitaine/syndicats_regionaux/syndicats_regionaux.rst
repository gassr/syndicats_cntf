.. index::
   pair: syndicats régionaux; Aquitaine

.. _syndicats_regionaux_aquitaine:

=======================================
Syndicats régionaux Aquitaine
=======================================

.. toctree::
   :maxdepth: 4

   ptt_aquitaine/ptt_aquitaine
   stpa/stpa
