
.. index::
   pair: syndicat régional; STPA
   ! STPA


.. _stp_aquitaine:

============================================================
Syndicat des Travailleurs et Précaires de l'Aquitaine (STPA)
============================================================

.. seealso::

   - http://www.cntaquitaine.ouvaton.org/index.html
   - http://www.cntaquitaine.ouvaton.org/page5.html


Adresse
=======

::

	Syndicat des travailleurs et précaires d’aquitaine
	chemin de boutique
	40500 Saint-sever
	tel 05 58 76 02 04
	https://fr.wikipedia.org/wiki/Saint-Sever


:Adresse courriel: cnt40@wanadoo.fr
