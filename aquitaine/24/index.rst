
.. index::
   pair: syndicats; CNT Dordogne (24)
   pair: Union départementale; UD24
   ! Dordogne



.. _ud24_publique:

=========================================
Union départementale Dordogne (UD24)
=========================================

.. seealso::

   - :ref:`union_regionale_aquitaine`
   - http://www.cnt-f.org/fedeptt/spip.php?article80


Syndicats de Dordogne
==========================

.. toctree::
   :maxdepth: 4

   interco24/index
