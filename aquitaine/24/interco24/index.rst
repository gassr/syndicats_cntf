
.. index::
   pair: syndicat; interco24
   pair: interco; 24


.. _interco_24_pub:

=======================================
Syndicat CNT interco24 (Périgueux)
=======================================

.. seealso::

   - :ref:`ud24_publique`


Activites interco24
===================

.. toctree::
   :maxdepth: 4

   activites/index
