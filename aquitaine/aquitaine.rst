
.. index::
   pair: syndicats; CNT Aquitaine
   pair: Union Régionale ; Aquitaine
   ! Aquitaine


.. _union_regionale_Aquitaine:

=======================================================
Union Régionale Aquitaine
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Aquitaine
   - http://www.cnt-f.org/spip.php?article38


.. figure:: aquitaine_region_locator_map.svg.png

   *L'Aquitaine*


:code région: 42


::

    36 rue Sanche de Pommiers
    33000 Bordeaux
    Tel : 05 56 31 12 73
    Fax : 05 57 89 21 72


L'Aquitaine est une région historique et administrative du Sud-Ouest de la
France. Elle comprend cinq départements:

- Dordogne (24)
- Gironde (33)
- Landes (40)
- Lot-et-Garonne (47)
- Pyrénées-Atlantiques (64)

Son chef-lieu, Bordeaux, en est aussi la plus grande ville.

L'Aquitaine est également une région historique, dont les contours ont évolué
au cours du temps.

Ses habitants sont les Aquitains.


Syndicats régionaux Aquitaine
=============================

.. toctree::
   :maxdepth: 4

   syndicats_regionaux/syndicats_regionaux


Unions départementales Aquitaine
=================================

.. toctree::
   :maxdepth: 4


   24/index
   33/33
   40/index


Coopérative Aquitaine
=============================

.. toctree::
   :maxdepth: 4

   cooperatives/index
