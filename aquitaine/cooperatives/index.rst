
.. index::
   pair: Cooperatives; Aquitaine


.. _cooperatives_aquitaine:

=======================================
Cooperatives Aquitaine
=======================================

.. seealso:: http://www.cnt-f.org/cooperatives/

.. toctree::
   :maxdepth: 4

   coopequita/index
   sel_de_la_terre/index
