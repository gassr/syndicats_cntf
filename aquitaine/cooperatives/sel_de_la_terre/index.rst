
.. index::
   pair: Cooperatives; Sel de la terre


.. _sel_de_la_terre_aquitaine:

=======================================
Sel de la terre
=======================================

.. seealso::

   - http://seldelaterre.ouvaton.org/
   - http://www.biocoherence.fr



«Sel de la Terre »Association à but non lucratif pour le développement
de structures alternatives et autogestionnaires d'approvisionnement,
gérée par des bénévoles, afin de promouvoir l’agriculture naturelle, le
commerce solidaire et les productions respectueuses de l'environnement
et des personnes.

La coopérative est née fin 2010 à Pessac (33) dans le but de développer
notre vision des circuits courts et d’aider des producteurs CNT ou
proche de notre démarche.

Le nom vient en hommage au film :Sel de la terre (Salt of the Earth,
USA, 1954) de Herbert J. Biberman.

Deux fois par mois, nous tenons un marché de 16h à 20h sur la cité des
Castors où les ouvriers dans les années 50 ont construit eux-mêmes leurs
maisons, pour nous c’est un honneur qu’ils aient accepté de nous accueillir.

Au-delà des marchés nous avons tenu des stands dans plusieurs villes sur
l’Aquitaine pour nous faire connaître.

La coopérative est soutenue et portée par des militants cénétistes des
syndicats CNT-PTT et CNT-STP aquitaine

A l’heure actuelle 10 à 12 producteurs nous accompagnent, nous sommes
proches du réseau « accueil paysan » pour proposer lors d’événements des
stands à d’autres producteurs.

La coopérative s’inscrit dans la démarche de proposer des produits de
qualité, à des prix accessibles pour tous, d’aider dans la sauvegarde
des races anciennes tout cela dans l’esprit de l’éducation populaire.

Acheter à la coopérative n’est pas un acte militant nous nous devons
pour la clientèle (quelle soit cenetiste, coopérative ou autres…) de
pouvoir lui assurer une qualité des produits, une garantie d’élevages,
de soins et d’abatages qui ne soient pas uniquement réalisés sur la
parole des producteurs même si nous faisons entièrement confiance à nos
producteurs présents à la coopérative pour les avoir vu travailler où
pour les avoir aider.

Si certains producteurs ont le label AB , d’autres le refusent , nous
suivons de près l’actualité du label `bio cohérence`_
pour voir son évolution .

La coopérative s’inscrit naturellement dans le combat contre les OGM ,
dans la défense des petits abattoirs qui risquent de disparaître comme
la paysannerie , le refus de la puce électronique n’est pas le refus de
la traçabilité une solution alternative doit être trouvé à la puce
électronique pour ceux qui souhaitent la refuser .


.. _`bio cohérence`: http://www.biocoherence.fr
