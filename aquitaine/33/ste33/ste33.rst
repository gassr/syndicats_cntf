
.. index::
   pair: syndicat; STE33
   pair: STE; 33


.. _ste_33_pub:
.. _ste33:

=======================================
Syndicat CNT STE33
=======================================

.. seealso::

   - :ref:`ud33_publique`


Motions STE33
===================

.. toctree::
   :maxdepth: 4

   motions_congres/motions_congres
