
.. index::
   pair: Union Locale; Bordeaux
   ! Bordeaux


.. _ul_bordeaux_pub:

=======================================================================================
Union locale bordeaux (UL33)
=======================================================================================

.. seealso::

   - :ref:`ud33_publique`
   - http://www.cnt-f.org/ul33/


::

	36 rue Sanche de Pommiers
	33000 Bordeaux
	tel : 05 56 31 12 73
	mail : ul33@cnt-f.org
	web : http://www.cnt-f.org/ul33/
	Permanence le samedi de 14 h à 16 h
