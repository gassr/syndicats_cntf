
.. index::
   pair: syndicat; interco33
   pair: interco; 33


.. _interco_33_pub:

=======================================
Syndicat CNT interco33
=======================================

.. seealso::

   - :ref:`ud33_publique`

Adresse
=======

::

	Syndicat Interco 33
	36, rue Sanche-de-Pomiers
	33000 bordeaux
	Tel : 05 56 31 12 73


:Adresse courriel: interco33@cnt-f.org


Activites interco33
===================

.. toctree::
   :maxdepth: 4

   activites/index
