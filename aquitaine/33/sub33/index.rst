
.. index::
   pair: syndicat; SUB33
   ! SUB33


.. _sub_33_pub:

=======================================
Syndicat CNT SUB33
=======================================

.. seealso::

   - :ref:`ud33_publique`



Activités
=========

.. toctree::
   :maxdepth: 4

   activites/index
