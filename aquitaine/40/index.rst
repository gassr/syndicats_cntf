
.. index::
   pair: syndicats; CNT Landes (40)
   pair: Union départementale; UD40
   ! Landes


.. _ud40_publique:

=======================================
Union Départementale Landes (UD40)
=======================================


.. seealso::

   - https://fr.wikipedia.org/wiki/Landes_%28d%C3%A9partement%29
   - http://www.cntaquitaine.ouvaton.org/


Le département des Landes (las Lanas en gascon) est un département français
appartenant à la région Aquitaine.



::

	Lieu dit "Boutique"
	Quartier Augreilh
	40500 St Sever
	tel : 05 58 76 02 04
	mail : cnt40@wanadoo.fr
	web : http://www.cntaquitaine.ouvaton.org/
