
.. index::
   pair: syndicats; CNT Corse
   pair: Union Régionale ; Corse
   ! Corse


.. _union_regionale_corse:

=======================================================
Union Régionale Corse
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Corse


.. figure:: corse.png

   *La région Corse*


:code région: 94


La collectivité territoriale de Corse (CTC) est l'une des 22 collectivités de
niveau régional de France métropolitaine, correspondant au territoire de la Corse.

Depuis la loi du 13 mai 1991, elle dispose d'un statut particulier au sein de
la République française qui lui confère davantage de pouvoir que les régions
continentales, comme elle métropolitaines

- Corse-du-Sud (2A)
- Haute-Corse (2B)
