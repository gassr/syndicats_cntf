
.. index::
   pair: syndicat régional; ETPRECI35


.. _etpreci35:

====================
ETPRECI35
====================

Motions congrès
================

.. toctree::
   :maxdepth: 4

   motions_congres/index
