
.. index::
   pair: Twitter; UD35


.. _lad_cnt:

============================
https://twitter.com/LAD_CNT
============================

.. seealso::

   - unlocalpourlafau.noblogs.org


Lutte de la CNT Rennes-2 pour défendre son local syndical, menacé d'expulsion
en janvier 2014.
