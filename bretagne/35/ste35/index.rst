
.. index::
   pair: syndicat régional; STE35


.. _ste35:

====================
STE35
====================

Motions congrès
================

.. toctree::
   :maxdepth: 4

   motions_congres/index
