
.. index::
   pair: syndicats; CNT Bretagne
   pair: Union Régionale ; Bretagne
   ! Bretagne


.. _union_regionale_bretagne:

=======================================================
Union Régionale Bretagne (Breizh (br) - Bertaèyn (ga))
=======================================================

.. seealso::

   - http://www.cnt-f.org/spip.php?article34 (Bretagne)
   - https://fr.wikipedia.org/wiki/Bretagne
   - :ref:`union_regionale_pays_de_la_loire`


.. figure:: bretagne.png

   *La bretagne*


:code région: 53




Introduction
=============

La Bretagne, parfois appelée Bretagne historique pour la différencier
de la région Bretagne, est une péninsule de l'ouest de la France,
située entre la Manche au nord, la mer Celtique et d'Iroise à l'ouest
et le golfe de Gascogne au sud.

À la fin de l'Empire romain, elle connaît un afflux de population due à
l'immigration massive  de Bretons insulaires dans une partie de
l'ancienne Armorique celte. Ceux-ci créent un royaume[ au IXe siècle,
qui devient ensuite un duché. Elle devient en 1532 une « province réputée étrangère »
unie à la France sous la même couronne jusqu'à sa disparition
administrative en 1790 et sa division en cinq départements :

- Côtes-d'Armor (22)
- Finistère (29)
- Ille-et-Vilaine (35)
- Morbihan (56)

La Bretagne possède une identité forte, pourtant encore soumise à controverse.


Adresse UR
===========

::

	CNT - Union Régionale Bretagne et Pays de la Loire
	C/O B17 - 17, rue Paul Bellamy
	44000 Nantes
	bretagne@cnt-f.org


Départements
=============

.. toctree::
   :maxdepth: 4

   29/29
   35/index


Syndicats régionaux Bretagne
============================

.. toctree::
   :maxdepth: 4

   syndicats_regionaux/index
