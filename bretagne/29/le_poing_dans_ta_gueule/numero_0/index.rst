
.. index::
   pair: Le Poing Dans Ta Gueule; Numéro 0 (juillet 2007)


.. _le_poing_dans_ta_gueule_0:

===============================================
Le Poing Dans Ta Gueule numéro 0, juillet 2007
===============================================

.. seealso::

   - http://www.cnt-f.org/staf/wp-content/uploads/2010/01/Le-P0-v1-2.pdf
   - http://www.travail.gouv.fr/informations-pratiques/fiches-pratiques/91.html



- un été meurtrier pour nos droits sociaux.
- des travailleurs comme les autres
- Fiche pratique du mois :  **Le contrat de travail temporaire**
