
.. index::
   pair: Le Poing Dans Ta Gueule; Numéro 5(janvier 2012)


.. _le_poing_dans_ta_gueule_5:

===============================================
Le Poing Dans Ta Gueule Numéro 4, janvier 2012
===============================================

.. seealso::

   - http://www.cnt-f.org/staf/wp-content/uploads/2011/12/t%C3%A9l%C3%A9charger-le-PDG-n%C2%B05.pdf


Triple zéro
===========

L’économie de demain sera celle que les peuples choisiront : Soit ils courbent
l’échine et leur sort ne peut qu’empirer, soit ils prennent conscience que sans
eux les rois ne sont rien et alors tout devient possible.


La défense du pouvoir d’achat : un consensus trompeur
=====================================================

Nous ne voulons pas d’une vie de riche ; nous voulons une vie riche.

Un salaire pour quoi faire ?
=============================

« De chacun selon ses moyens, à chacun selon ses besoins »

Il était une fois... le salariat
=================================

.. seealso:: http://www.dailymotion.com/video/xu1ac_histoire-du-salariat-1_shortfilms#from=embed

L’espoir
--------

1906-1975


Le temps du doute
-----------------

.. seealso:: http://www.dailymotion.com/video/xu26a_histoire-du-salariat-2

1976-2006

Collectif pour un autre futur Buenaventura Durruti nato 3164/3244 (réédition 2011)
==================================================================================

::

	Buenaventura Durruti
	nato 3164/3244 (réédition 2011)

"Dès mon plus jeune âge, la première chose que j’ai perçue autour de moi, c’est
la souffrance, non seulement celle de notre famille, mais aussi celle de mes
voisins. Par intuition, j’étais déjà un rebelle. Je crois que mon destin s’est
décidé alors."

Buenaventura Durruti.


Quarante années de luttes, d'exils, d'attentats, d'emprisonnements, d'activités
clandestines, de grèves, d'insurrection, Buenaventura Durruti (né le 14 juillet
1896, tué le 20 novembre 1936) a vécu plusieurs vies, toutes exprimant au plus
profond, le même désir de libération de la personne humaine.

Le chemin est long pour le jeune ouvrier révolté devenant l'homme qui refuse
postes, honneurs, grades et dont la mort fut pleurée par des millions de
femmes et d'hommes. L'histoire de ce libertaire sans compromission, ce
révolutionnaire intransigeant, se confond avec celles de la révolution et
de la guerre d'Espagne, clés indispensables à la compréhension de l'histoire du
XXème siècle et de sa suite.

En deux disques et 36 plages, musiciens, chanteurs, écrivains, acteurs
s'interrogent et créent à partir de l'histoire de Buenaventura Durruti,
une dédicace sans nostalgie et pleine d'actualité. Avec la participation d'Abel
Paz, auteur de la biographie « Buenaventura Durruti, un anarchiste espagnol ».

L'édition 2011, richement illustrée de nombreuses photographies d'époque, est
dotée du texte original enrichi et augmenté d'autres écrits trilingues (français,
espagnol, anglais) de Emma Goldman, de Carl Einstein ainsi que d'une nouvelle
préface situant cet enregistrement aujourd'hui.



::

	C’est seulement en se libérant de la peur que la société pourra s’identifier
	dans la liberté.

	Buenaventura Durruti.
