
.. index::
   pair: Le Poing Dans Ta Gueule; Numéro 1 (octobre 2007)


.. _le_poing_dans_ta_gueule_1:

===============================================
Le Poing Dans Ta Gueule numéro 1, octobre 2007
===============================================

.. seealso::

   - http://www.cnt-f.org/staf/wp-content/uploads/2010/01/PDGn%C2%B01.pdf


- A propos de sécurité sociale et de charges...
- Qui en a entendu parler de ce côté du Rhin ?
- Fiche pratique du mois : «Le départ négocié et la transaction »

Qui en a entendu parler de ce côté du Rhin ?
=============================================

À Nordhausen, petite ville de Thuringe, en plein cœur de
l’Allemagne, les 135 ouvriers d’une usine de cycles fermée
à la suite d’obscures magouilles financières, ont décidé
de continuer la production en autogestion.

L’histoire est à la fois compliquée et très classique : Le fonds
de pensions américain LONE STAR s’allie avec le fabricant de
vélos allemand MITTA pour racheter et fermer les unités de
production de l’entreprise concurrente BIRIA. On crée des
sociétés-écrans qui achètent et se revendent les pièces du
puzzle, on fait en sorte que plus personne ne comprenne qui
est le propriétaire de quoi, on absorbe BIRIA, on lui refile les
dettes du nouveau groupe, on déclare l’entreprise en faillite, et
le tour est joué : L’allemand MITTA n’a plus de concurrent et
l’américain LONE STAR s’est engraissé au passage.

Mais c’était compter sans la détermination des ouvriers
de l’unité BIRIA de Nordhausen. Soutenus par le syndicat
libertaire FAU, ils ont refusé la fermeture de leur usine et ont
décidé de poursuivre la production, sans patrons cette fois.

En s’appuyant sur le réseau de solidarité internationale de la
FAU, ils ont lancé le défi STRIKE-BIKE : Obtenir avant le 2
octobre un minimum de 1800 commandes prépayées. Opération
réussie, avec des commandes provenant de toute l’Europe,
mais aussi d’Égypte, des Etats-Unis, d’Australie, du Canada,
d’Afrique du Sud et d’Israël. Le 23 octobre, la production pourra
commencer.

L’histoire de STRIKE-BIKE est exemplaire : Contre les
manœuvres du patronat allié aux groupes financiers et malgré
l’occultation quasi-totale de l’affaire par les médias, la
détermination des travailleurs et la solidarité internationale
permettent de remporter des victoires.
www.strike-bike.de
