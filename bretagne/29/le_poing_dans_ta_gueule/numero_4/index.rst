
.. index::
   pair: Le Poing Dans Ta Gueule; Numéro 4 (mai 2011)


.. _le_poing_dans_ta_gueule_4:

===============================================
Le Poing Dans Ta Gueule Numéro 4, mai 2011
===============================================

.. seealso::

   - http://www.cnt-f.org/staf/wp-content/uploads/2011/04/T%C3%A9l%C3%A9charger-le-PDG-n%C2%B04-A5-V1.pdf


le travail c’est la santé
=========================

Un mot n’est jamais neutre. Il transporte avec lui l’histoire des générations
qui l’ont forgé et même si on n’en a plus clairement conscience, cette histoire
est toujours là ...

Casseurs ou sauveurs ?
======================

Le travail pour quoi faire ? Pour gagner  sa vie, certes ! Mais pas à n’importe
quel prix, pas dans n’importe quelles conditions de travail,…

C’est nous qui TRAVAILLONS, C’EST NOUS QUI DECIDONS !
=====================================================

Le slogan :  « De l’autogestion des luttes à l’autogestion so­ciale » résume à
la fois notre  pratique syndicale et notre projet de société....
