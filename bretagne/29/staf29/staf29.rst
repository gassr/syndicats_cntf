
.. index::
   pair: syndicat; staf29
   ! Quimper


.. _staf_29_pub:
.. _staf29:

=============================================================================================
**Syndicat CNT STAF29** Syndicat des Travailleurs Autogestionnaires du Finistère (Quimper)
=============================================================================================

.. seealso::

   - :ref:`ud29_publique`
   - http://www.cnt-f.org/staf/


::

	CNT29-STAF
	BP 31507
	29105 QUIMPER
    Tel: 06 86 67 53 83


STAF, c’est quoi ?
==================

Ce site est géré par les syndiqués C.N.T. de la région de Quimper : le
STAF-CNT29 regroupe les syndiqués du sud Finistère de différents secteurs
d’activités ( industrie, service à la personne, éducation, PTT,...), pour le
nord Finistère une autre union locale , l’Interco 29, est présente sur Brest.

La raison d’être de ce site est de vous informer sur nos activités locale et
notre actualité, pour connaître les objectifs et la position de la CNT dans
ses différents champs d’action, rendez vous sur le site de la confédération :
Luttes internationales, autogestion, démocratie directe, action directe ...

Comment ça fonctionne ?
-----------------------

« L’encartage » n’est pas un principe d’adhésion à la CNT, c’est plutôt sur le
terrain des luttes que se créent d’ordinaire les contacts, afin de vous
éclairer sur notre fonctionnement nous vous apportons ici quelques
« principes et orientations » de notre syndicat.

Il est peut être important de rappeler que par principe et selon les statuts,
on adhère à la CNT de manière « géographique » (en fonction de l’implantation )
auprès de l’Union  Locale de son lieu de travail ou de résidence.

Le syndicat « Interco », le STAF-CNT29,  regroupe des salariés de tous les
secteurs d’activité c’est  la structure d’accueil locale en l’absence d’un
syndicat CNT « spécialisé » dans un secteur.

Le rôle des  fédérations est d’informer les adhérents  des activités plus
propres à leur secteur professionnel et de favoriser leur regroupement pour
la défense active de leurs intérêts communs.


Activités
=========

.. toctree::
   :maxdepth: 4

   activites/activites

Motions
=========

.. toctree::
   :maxdepth: 4

   motions_congres/motions_congres
