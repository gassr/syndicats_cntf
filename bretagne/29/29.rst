
.. index::
   pair: syndicats; CNT Finistère (29)
   pair: Union départementale; UD29
   ! 29
   ! Finistère


.. _ud29_publique:

=======================================
Union Départementale Finistère (UD29)
=======================================


Syndicats CNT Finistère(29) .

.. seealso::

   - http://www.cnt-f.org/staf
   - :ref:`union_regionale_bretagne`

::

	Syndicats CNT 29
	BP 31507 29105 Quimper cedex ;
	ud.29@cnt-f.org ;
	06 86 67 53 83
	courriel ud.29@cnt-f.org


L'Union départementale 29 est constituée des syndicats suivants:

- :ref:`staf29 <staf_29_pub>`


Publication
===========

.. toctree::
   :maxdepth: 4


   le_poing_dans_ta_gueule/index

Luttes
======

.. toctree::
   :maxdepth: 4

   luttes/index


Syndicats
=========

.. toctree::
   :maxdepth: 4

   staf29/staf29
