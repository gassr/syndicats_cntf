
.. index::
   pair: syndicat régional; Chimie BZH


.. _chimie_bzh:

====================
Syndicat Chime BZH
====================



Adresse courriel: chimieb@cnt-f.org
===================================

- chimieb@cnt-f.org

Motions congrès
================

.. toctree::
   :maxdepth: 4

   motions_congres/index
