
.. index::
   pair: syndicats; CNT Basse-Normandie
   pair: Union Régionale ; Basse-Normandie
   ! Basse-Normandie


.. _union_regionale_basse_normandie:

=======================================================
Union Régionale Basse-Normandie
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Basse-Normandie


.. figure:: basse_normandie.png

   *La basse normandie*


:code région: 25


La Basse-Normandie correspond à la partie occidentale de l'ancienne province de
Normandie et à la partie nord de l'ancien comté du Perche.

Elle est bordée au nord et à l'ouest par la mer de la Manche, au nord-est par la
région Haute-Normandie, au sud-est par la région Centre, au sud par la région
Pays de la Loire et au sud-ouest, sur une petite portion, par la Bretagne.

Elle compte 3 départements:

- Calvados (14)
- Manche (50)
- Orne (61)
