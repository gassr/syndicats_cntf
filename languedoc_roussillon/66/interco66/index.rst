
.. index::
   pair: syndicat; interco66


.. _interco_66_pub:

=======================================
Syndicat CNT interco66 (Perpignan)
=======================================

.. seealso::

   - :ref:`ud66_publique`
   - http://www.cnt-f.org/cnt66/spip.php?rubrique6
