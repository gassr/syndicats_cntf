
.. index::
   pair: syndicats; CNT Languedoc-Roussillon
   pair: Union Régionale ; Languedoc-Roussillon
   ! Languedoc-Roussillon


.. _union_regionale_lr:
.. _syndicats_languedoc_roussillon:

=======================================
Union Régionale Languedoc-Roussillon
=======================================

.. seealso::

   - http://www.cnt-f.org/spip.php?article28
   - https://fr.wikipedia.org/wiki/Languedoc-Roussillon

.. figure:: Region_Languedoc-Roussillon_(logo).svg.png

   *Logo Languedoc-Roussillon*


:code région: 91

Le Languedoc-Roussillon est une région française composée de cinq
départements:

- Aude (11)
- Gard (30)
- Hérault (34)
- Lozère (48)
- Pyrénées-Orientales (66)

Elle est bordée au sud par l'Espagne, Andorre et la mer Méditerranée
(le golfe du Lion), et les régions françaises suivantes :

- Provence-Alpes-Côte d'Azur
- Rhône-Alpes,
- Auvergne,
- Midi-Pyrénées.


.. toctree::
   :maxdepth: 4

   30/30
   34/34
   48/48
   66/66

Adresse
=======

::

    UR CNT Languedoc-Roussillon
    6, rue d’Arnal
    30000 NIMES
    Mail : ur.lr@cnt-f.org
