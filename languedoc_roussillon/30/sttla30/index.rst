
.. index::
   pair: syndicat; sttla30



.. _sttla_30_pub:

=======================================================================================================
Syndicat des Travailleurs des Transports de la Logistique et des Activités auxiliaires du Gard (Nîmes)
=======================================================================================================

.. seealso::

   - :ref:`ud30_publique`
   - :ref:`federation_des_transports`


::

	Syndicat des travailleurs des transports de la logistique et des Activités auxiliaires du Gard
	6 rue d’arnal 30900 Nîmes
	Réunion tout les 1ers vendredi du mois.
	Mail : sttla30@cnt-f.org


Activités
=========

.. toctree::
   :maxdepth: 4

   activites/index
