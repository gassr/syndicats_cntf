
.. _etpic30_motions_congres:
.. _motions_etpic30_congres:
.. _motions_etpic30:

========================================
Motions congrès ETPIC30
========================================

.. toctree::
   :maxdepth: 3

   2021/2021
   2014/2014
   2012/2012
   2010/2010
   2008/2008
