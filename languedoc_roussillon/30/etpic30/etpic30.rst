
.. index::
   pair: syndicat; etpic30
   pair: 30; etpic30


.. _etpic_30_pub:
.. _etpic30_pub:
.. _etpic30:
.. _etpic_30:

=======================================
Syndicat CNT etpic30 (Nîmes)
=======================================

.. seealso:: :ref:`ud30_publique`


::

    CNT ETPIC du Gard sud
    6, rue d’Arnal
    30000 Nîmes
    Tél. : 09 50 07 60 88
    Mail : cnt.nimes@cnt-f.org


Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres


Activités
=========

.. toctree::
   :maxdepth: 4

   activites/index
