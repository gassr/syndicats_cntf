
.. index::
   pair: syndicats; CNT Picardie
   pair: Union Régionale ; Picardie
   ! Picardie


.. _union_regionale_picardie:

=======================================================
Union Régionale Picardie
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Picardie


.. figure:: picardie.png

   *La région Picardie*


:code région: 22


- Aisne (02)
- Oise (60)
- Somme (80)
