
.. index::
   pair: syndicat; interpro67



.. _interpro_67_pub:

=======================================================================================
Syndicat interprofessionel du Bas-Rhin (interpro67, Strasbourg)
=======================================================================================

.. seealso::

   - :ref:`ud67_publique`
   - http://www.cnt-f.org/cnt67/v2/les-syndicats/

::

	Interpro 67 : Syndicat interprofessionel du bas rhin
	contact : interpro67.at.cnt-f.org
