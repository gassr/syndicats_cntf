
.. index::
   pair: syndicat; STEA
   pair: syndicats 67; STEA


.. _stea67_pub:

=======================================================================================
Syndicat STE-Alsace 67
=======================================================================================

.. seealso::

   - :ref:`ud67_publique`
   - http://www.cnt-f.org/fte/?-CNT-STE-Alsace-67,346-



:Adresse électronique: stea AT cnt-f.org

::

    Site internet : http://www.cnt-alsace.org
    Adresse postale c/o Lucha y Fiesta -
    BP : 30017
    67027 STRASBOURG CEDEX 1
