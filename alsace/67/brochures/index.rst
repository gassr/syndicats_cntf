
.. index::
   pair: publications; CNT Bas-Rhin (67)

.. _brochures_ud67:

=========================================
Brochures CNT Bas-Rhin (UD67)
=========================================

.. seealso:: http://www.cnt-f.org/cnt67/v2/brochures/

Pleinement engagée au sein des luttes sociales, la CNT 67 a souhaité par le
biais de la diffusion de textes militants, faire sienne la phrase de
Fernand Pelloutier **instruire pour révolter »**

Afin de permettre aux travailleuses / eurs de mieux comprendre leur situation
notamment par la lecture, cette diffusion se devait d’être accessible à tous,
exempte de tous soucis de rentabilité, c’est pourquoi ces documents sont
disponibles en prix libre et en téléchargement libre et gratuit sur ce site.


Mémoire d’un ouvrier en Espagne – B. Martinez
=============================================

.. seealso:: http://www.cnt-f.org/cnt67/v2/wp-content/uploads/Brochure-compl%C3%A8te-Memoires-Espagne-1.pdf


L’organisation et le programme anarchiste – E. Malatesta
========================================================

.. seealso:: http://www.cnt-f.org/cnt67/v2/wp-content/uploads/Broch-int%C3%A9rieure-Orga-et-Prg-Malatesta-1.pdf


Quand il faut s’organiser – A. Flood (pdf)
==========================================

.. seealso:: http://www.cnt-f.org/cnt67/v2/wp-content/uploads/Broch-int%C3%A9rieure-qd-il-faut-sorganiser.pdf
