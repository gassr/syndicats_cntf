
.. index::
   pair: syndicat; interco68



.. _interpro_68_pub:

=======================================================================================
Syndicat interco du Haut-Rhin (interco68, Strasbourg)
=======================================================================================

.. seealso::

   - http://www.cnt-f.org/spip.php?article39
   - http://www.cnt-f.org/cnt67/v2/les-syndicats/
   - :ref:`ud68_publique`


Adresse
=======


::

	3 rue de St Amarin
	68690 Geishouse
	Tél : 06 81 16 46 32.
	Email : valerie.burger@orange.fr


Sections
========

.. toctree::
   :maxdepth: 4

   sections/index
