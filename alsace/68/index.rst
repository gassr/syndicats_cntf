
.. index::
   pair: syndicats; CNT Haut-Rhin (68)
   pair: Union départementale; UD68
   ! Haut-Rhin


.. _ud68_publique:

=========================================
Union Départementale Haut-Rhin (UD68)
=========================================


.. seealso::

   - http://www.cnt-alsace.org/
   - https://fr.wikipedia.org/wiki/Haut-Rhin
   - :ref:`union_regionale_alsace`


Le Haut-Rhin est un département français faisant partie de la région Alsace.

La préfecture ainsi que le Conseil général du Haut-Rhin sont basés à Colmar,
ce qui en fait le siège administratif tandis que Mulhouse est la ville la plus
peuplée du département, près de 40 % de la population totale du Haut-Rhin vit
dans son aire urbaine. C'est également la 25e ville de France en termes de
population contenue dans son unité urbaine.


Syndicats
==============

.. toctree::
   :maxdepth: 4

   interco68/index
