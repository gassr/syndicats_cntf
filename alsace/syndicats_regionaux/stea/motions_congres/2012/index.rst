

.. _motions_stea_2012:

========================================
Motions congres STEA 2012
========================================

- :ref:`motion_5_2012`
- :ref:`motion_15_2012`
- :ref:`motion_16_2012`
- :ref:`motion_21_2012`


.. _amendements_stea_2012:

Amendements  STEA 2012
=======================

- :ref:`amendement_motion_2_2012_stea`
