
.. index::
   pair: syndicat régional; STEA


.. _stea:

============================================================
Syndicat STEA
============================================================

Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/index
