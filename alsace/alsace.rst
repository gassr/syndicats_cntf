
.. index::
   pair: syndicats; CNT Alsace
   pair: Union Régionale ; Alsace
   ! Alsace


.. _union_regionale_alsace:

=======================================================
Union Régionale Alsace
=======================================================

.. seealso::

   - http://www.cnt-f.org/spip.php?article39
   - https://fr.wikipedia.org/wiki/Alsace
   - http://www.cnt-alsace.org/


.. figure:: alsace_region_locator_map.svg.png
   :align: center

   *Alsace*


:code région: 42

L'Alsace est une région culturelle, historique, et administrative du nord-est
de la France métropolitaine.

Région de l'Europe rhénane, elle se trouve au cœur de la mégalopole européenne
Avec une densité de 223 hab./km2, c'est la troisième région la plus densément
peuplée de France métropolitaine2 après l'Île-de-France et le Nord-Pas-de-Calais
mais c'est aussi la plus petite par sa superficie.

L’Alsace est la première région exportatrice française en valeur d'exportations
par habitant, la deuxième quant au revenu disponible brut des ménages, enfin,
c'est une des régions de France où le taux de chômage est le plus bas.

Ce tableau idyllique ne doit pas masquer l'apparition de difficultés
économiques depuis le début des années 2000.

Aujourd'hui, l’Alsace est divisée en deux départements:

- Bas-Rhin (67)
- Haut-Rhin (68)

Le conseil régional siège à Strasbourg, qui est aussi la plus importante
(449 798 habitants) des cinq grandes agglomérations de la région devant
Mulhouse (243 618 habitants), Colmar (90 842 habitants),
Haguenau (58 937 habitants) et Saint-Louis (Banlieue française de Bâle - 36 225 habitants).

Deux Alsaciens sur trois vivent au sein de ces cinq aires urbaines.

Strasbourg et Mulhouse sont respectivement les 11e et 25e agglomérations les
plus peuplées de France.

De tradition industrielle forte, Mulhouse est la ville de France métropolitaine
qui a la plus forte proportion de jeunes tandis que Strasbourg est le siège de
plusieurs institutions européennes, dont le Parlement européen
et le Conseil de l'Europe.


Unions départementales Alsace
=============================

.. toctree::
   :maxdepth: 4

   67/67
   68/index


Syndicats régionaux
===================

.. toctree::
   :maxdepth: 3

   syndicats_regionaux/index
