
.. index::
   pair: Union Locale; Besançon
   ! Besançon


.. _ul_besancon_publique:

=======================================
Union Locale Besançon
=======================================

.. seealso::

   - https://cntbesancon.wordpress.com/

Union Locale CNT Besançon

::

	c/o CESL BP 121
	25014 Besançon Cedex
	Mail : cnt-doubs@cnt-f.org
	Web : http://cntbesancon.wordpress.com/
