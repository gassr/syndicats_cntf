
.. index::
   pair: Syndicat; interco25
   ! interco25


.. _interco25_pub:
.. _interco25:

==============================================================================
Syndicat intercorporatif 25 (interco25)
==============================================================================




Motions Congrès interco25
==========================

.. toctree::
   :maxdepth: 4

   motions_congres/motions_congres
