
.. index::
   pair: Syndicat; interco nord franche-comté
   ! interco nord franche comté


.. _interco_nord_fc:

==============================================================================
CNT Interco Nord-Franche Comté (Montbéliard / Belfort)
==============================================================================

.. seealso::

   - http://www.cnt-f.org/spip.php?article31
   - https://fr.wikipedia.org/wiki/Belfort
   - https://fr.wikipedia.org/wiki/Montb%C3%A9liard
   - :ref:`ud90_publique`
   - :ref:`ul_besancon_publique`

Belfort
=======

Belfort (prononciation traditionnelle [befɔːʁ] ou [bɛfɔːʁ]1, en franc-comtois : Béfô)
est une ville et commune française. Elle fait partie de l'espace métropolitain
Rhin-Rhône et est située à 36 km à l'ouest de Mulhouse, dans la région Franche-Comté,
sur la Savoureuse.

Il s'agit de la plus grande ville et du chef-lieu du département du Territoire
de Belfort.

Montbéliard
===========

Montbéliard (en franc-comtois : Monbyai) est une commune française qui fait
partie de la métropole Rhin-Rhône, elle est située dans le département du Doubs
et la région Franche-Comté.


::

    CNT Interco Nord-Franche Comté (Montbéliard / Belfort)


écrire à l’:ref:`UL CNT de Besançon <ul_besancon_publique>`  qui transmettra.

:Mail: cnt-nord-fc@cnt-f.org
