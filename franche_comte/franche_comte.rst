
.. index::
   pair: syndicats; CNT Franche-Comté
   pair: Union Régionale ; Franche-Comté
   ! Franche-Comté


.. _union_regionale_franche_comte:

=======================================================
Union Régionale Franche-Comté
=======================================================

.. seealso::

   - http://www.cnt-f.org/spip.php?article31
   - https://fr.wikipedia.org/wiki/Franche-Comt%C3%A9


.. figure:: Franche-comte_administrative.svg.png
   :align: center

   *La Franche-Comté*


:code région: 43


La Franche-Comté (Franche-Comtât en francoprovençal, Fraintche-Comtè en
franc-comtois) est une région française qui regroupe quatre départements:

- Doubs (25)
- Jura (39)
- Haute-Saône (70)
- Territoire de Belfort (90)

Avec une surface de 16 202 km2, la région représente près de 3 % du territoire
français. Les deux pôles urbains de la régions sont la capitale régionale de
région, Besançon, et le pôle économique Belfort-Montbéliard.

Le nom de la région vient de l'expression franche comté de Bourgogne, comté
pouvant être du féminin en ancien français (la Bourgogne à proprement parler,
elle, était le duché de Bourgogne).

La Franche-Comté est une des rares régions françaises dont le territoire actuel
correspond sensiblement à une ancienne province royale, même si, depuis le
XIXe siècle, la région inclut aussi le Territoire de Belfort, qui était
initialement un pays traditionnel de langue d'oïl faisant partie de l'Alsace.

Aujourd'hui le territoire de la Franche-Comté est constitué, principalement,
de l'ancienne province de Franche Comté (ou Franche Comté de Bourgogne),
du comté de Montbéliard et du Territoire de Belfort, partie de l'Alsace non
annexée en 1871.

Cette relative unité historique, marquée par l'autonomie importante dont a
bénéficié la comté de Bourgogne par le passé (notamment sous les Habsbourg),
explique la forte identité de la région.



.. toctree::
   :maxdepth: 4

   25/25
   39/index
   90/index
   interco_nord_fc/index
