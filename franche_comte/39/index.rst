
.. index::
   pair: syndicats; CNT Jura (39)
   pair: Union départementale; UD39
   pair: Jura; 39
   ! Jura


.. _ud39_publique:

=======================================
Union Départementale Jura (UD39)
=======================================


.. seealso::

   - https://cntbesancon.wordpress.com/
   - https://fr.wikipedia.org/wiki/Jura_%28d%C3%A9partement%39
   - :ref:`union_regionale_franche_comte`

Le Jura est un département français dont le nom vient du massif du Jura.

Le département du Jura fait partie de la région Franche-Comté.

Il est limitrophe des départements du Doubs, de la Haute-Saône, de la Côte-d'Or,
de Saône-et-Loire et de l'Ain, ainsi que du canton de Vaud (Suisse).


Syndicats
=========

.. toctree::
   :maxdepth: 4

   interco39/index
