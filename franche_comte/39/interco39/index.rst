
.. index::
   pair: Syndicat; interco39
   ! interco39
   ! 39


.. _interco39_pub:

==============================================================================
Syndicat intercorporatif CNT Jura (interco39)
==============================================================================

.. seealso::

   - http://www.cnt-f.org/spip.php?article31
   - :ref:`ud39_publique`



Adresse postale
===============

::

  CNT Jura
  BP 98
  39140 BLETTERANS CC
