
.. index::
   pair: syndicats; CNT Territoire de Belfort (90)
   pair: Union départementale; UD90
   ! Territoire de Belfort
   pair: Territoire de Belfort; 90


.. _ud90_publique:

=================================================
Union Départementale Territoire de Belfort (UD90)
=================================================


.. seealso::

   - :ref:`union_regionale_franche_comte`
   - https://fr.wikipedia.org/wiki/Territoire_de_Territoire de Belfort


Le Territoire de Belfort ou Territoire-de-Belfort est un département français
créé en 1922 sur l'ancienne partie de l'Alsace restée à la France en 1871.

Par la suite, lors de la création des régions françaises en 1982, il a été
intégré à la région Franche-Comté.
