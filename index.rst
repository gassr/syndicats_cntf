
.. figure:: images/bandoconf.jpg
   :align: center


.. figure:: images/repartition_egalitaire_des_richesses.jpg
   :align: center


.. _syndicats_CNT:

=======================================
**Syndicats de la CNT-F**
=======================================


- http://cnt-f.org
- :ref:`cnt_statuts`
- :ref:`regles_organiques`
- :ref:`glossaire_cnt`
- :ref:`glossaire_juridique`
- https://gitlab.com/cnt-f/syndicats
- https://gitlab.com/cnt-f/syndicats/-/boards
- :ref:`genindex`


La France compte 22 régions en métropole (y compris la Corse) et 5 régions
outre-mer.


Meta
=====

.. toctree::
   :maxdepth: 2
   :caption: meta

   index/index
   meta/meta

Annuaire des syndicats
======================

.. seealso::

   - http://www.cnt-f.org/annuaire-des-syndicats-et-unions-locales-de-la-cnt.html


.. contents::
   :local:

Alsace
======

.. toctree::
   :maxdepth: 5

   alsace/alsace


Aquitaine
=========

.. toctree::
   :maxdepth: 5

   aquitaine/aquitaine

Auvergne
=========
.. toctree::
   :maxdepth: 5

   auvergne/index

Bourgogne
=========

.. toctree::
   :maxdepth: 9

   bourgogne/bourgogne

Bretagne
=========

.. toctree::
   :maxdepth: 5

   bretagne/bretagne


Basse Normandie
===============

.. toctree::
   :maxdepth: 5

   basse_normandie/index

Centre
=======

.. toctree::
   :maxdepth: 5

   centre/centre

Champagne Ardenne
=================

.. toctree::
   :maxdepth: 5

   champagne_ardenne/index

Corse
=========
.. toctree::
   :maxdepth: 5

   corse/index

Haute Normandie
===============

.. toctree::
   :maxdepth: 5

   haute_normandie/index


Franche Comté
==============

.. toctree::
   :maxdepth: 5

   franche_comte/franche_comte

Ile de France (Région Parisienne)
=================================

.. toctree::
   :maxdepth: 5

   ile_de_france/ile_de_france

Languedoc Roussillon
====================

.. toctree::
   :maxdepth: 5

   languedoc_roussillon/languedoc_roussillon

Limousin
=========

.. toctree::
   :maxdepth: 5

   limousin/index

Lorraine
=========

.. toctree::
   :maxdepth: 5

   lorraine/lorraine

Midi Pyrénées
=============

.. toctree::
   :maxdepth: 5

   midi_pyrenees/midi_pyrenees

Nord Pas-de-Calais
==================

.. toctree::
   :maxdepth: 5

   nord_pas_de_calais/nord_pas_de_calais

Pays de la Loire
================

.. toctree::
   :maxdepth: 5

   pays_de_la_loire/pays_de_la_loire


Picardie
=========

.. toctree::
   :maxdepth: 5

   picardie/index

Poitou Charentes
================

.. toctree::
   :maxdepth: 5

   poitou_charentes/index

Provence-Alpes-Côte d'Azur
============================

.. toctree::
   :maxdepth: 5

   provence_alpes_cote_dazur/provence_alpes_cote_dazur

Rhône-Alpes
===========

.. toctree::
   :maxdepth: 5

   rhone_alpes/rhone_alpes

Meta
=====

.. toctree::
   :maxdepth: 2
   :caption: Meta

   meta/meta
   index/index
