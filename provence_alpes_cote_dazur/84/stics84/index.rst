
.. index::
   pair: syndicat; stics84
   pair: stics; 84
   pair: Courriel; stics84@cnt-f.org


.. _stics_84_pub:

==========================================================================================================
Syndicat des travailleurs de l’Industrie et du  Commerce et des Services  du Vaucluse (stics84, Avignon)
==========================================================================================================

.. seealso::

   - :ref:`ud84_publique`
   - http://www.cnt-f.org/sam/spip.php?article15
   - http://cenetistes84.over-blog.com/article-87660559.html
   - :ref:`union_regionale_paca`


:Adresse courriel:  stics84@cnt-f.org


Le stics 84 est membre de :ref:`l'UR CNT Provence Alpes Côte-d'Azur <union_regionale_paca>`


.. figure:: stics84.png
   :align: center

   *Stics 84*

Blog
====

.. seealso:: http://cenetistes84.over-blog.com/article-87660559.html


Blog officiel du Syndicat des Travailleurs de l'Industrie, du Commerce et des
Services du Vaucluse de la Confédération Nationale du Travail.

Le stics 84 (syndicat des travailleurs de l'industrie, du commerce et des
services), est un syndicat affilié à la confédération nationale du travail.

Ce syndicat regroupe toutes les personnes du Vaucluses, rémunérées ou pas, en
activité ou non, qui ne sont ni patrons, ni délégataires de l'autorité d'iceux,
ni membres des forces répréssives,


.. index::
   pair: Videos; 1er mai 2012

Videos
======

1er mai 2012
------------

.. seealso:: http://www.cnt-f.org/video/videos/57-medias-communication-culture-spectacle-fetes/364-fete-de-la-cnt-vaucluse-le-1er-mai-2012-a-avignon


Permanences
===========

::

	Maison 4 de chiffre
	rue des teinturiers en Avignon


tous les 1er samedi du mois de 14h30 à 17h30.
