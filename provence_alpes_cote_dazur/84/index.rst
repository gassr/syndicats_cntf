
.. index::
   pair: syndicats; Vaucluse (84)
   pair: Union départementale; UD84
   ! Vaucluse


.. _ud84_publique:

=============================================
Union Départementale Vaucluse (UD84)
=============================================


.. seealso::

   - :ref:`union_regionale_paca`



Syndicats
=========

.. toctree::
   :maxdepth: 4

   stics84/index
