
.. index::
   pair: syndicat; stics83
   pair: stics; 83


.. _stics_83_pub:

==========================================================================================================
Syndicat des travailleurs de l’Industrie et du  Commerce et des Services  du Var (stics83, Toulon)
==========================================================================================================

.. seealso::

   - :ref:`ud83_publique`
   - http://www.cnt-f.org/sam/spip.php?article15


:Adresse courriel:  stics83@cnt-f.org
