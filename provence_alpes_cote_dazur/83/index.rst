
.. index::
   pair: syndicats; Var (83)
   pair: Union départementale; UD83
   ! Var


.. _ud83_publique:

=============================================
Union Départementale Var (UD83)
=============================================


.. seealso::

   - :ref:`union_regionale_paca`



Syndicats
=========

.. toctree::
   :maxdepth: 4

   stics83/index
