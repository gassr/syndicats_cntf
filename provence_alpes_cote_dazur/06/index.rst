
.. index::
   pair: syndicats; CNT Alpes-Maritimes (06)
   pair: Union départementale; UD06
   ! Alpes-Maritimes


.. _ud06_publique:

=============================================
Union Départementale Alpes-Maritimes (UD06)
=============================================


Syndicats CNT Alpes-Maritimes(06) .

.. seealso::

   - :ref:`union_regionale_paca`



Syndicats départementaux
========================

.. toctree::
   :maxdepth: 4

   sub06/index
