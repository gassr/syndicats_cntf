
.. index::
   pair: syndicat; SUB 06


.. _sub_06_pub:

==============================================
Syndicat SUB (Syndicat Unifié du Bâtiment) 06
==============================================

.. seealso:: :ref:`ud06_publique`



Activités
=========

.. toctree::
   :maxdepth: 4

   activites/index
