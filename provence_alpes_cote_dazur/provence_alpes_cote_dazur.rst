
.. index::
   pair: syndicats; CNT Provence-Alpes-Côte d'Azur
   pair: Union Régionale ; Provence-Alpes-Côte d'Azur
   ! Provence-Alpes-Côte d'Azur


.. _union_regionale_paca:

=======================================================
Union Régionale Provence-Alpes-Côte d'Azur (PACA)
=======================================================

.. seealso::

   - http://www.cnt-f.org/sam/spip.php?article15
   - https://fr.wikipedia.org/wiki/Provence-Alpes-C%C3%B4te_d%27Azur


.. figure:: provence-alpes-cote_dazur_region_locator_map.svg.png
   :align: center

   *La Provence-Alpes-Côte d'Azur*




:code région: 93
:Adresse courriel:  ur-paca@cnt-f.org

La Provence-Alpes-Côte d'Azur (Provença-Aups-Còsta d'Azur ou
Prouvènço-Aup-Costo d'Azur en occitan provençal, prononcé [pʀuˈvɛⁿsɔ ˈaw ˈkɔstɔ d aˈzyʀ])
est une région administrative française du Sud-Est.

Elle est souvent désignée par l'acronyme PACA.

Elle est limitrophe de l'Italie dont elle est séparée par les Alpes méridionales.
Au nord, elle voisine avec la région Rhône-Alpes et à l'ouest avec le
Languedoc-Roussillon dont le Rhône marque la limite.

La région PACA est baignée au sud par la mer Méditerranée.

La région Provence-Alpes-Côte d'Azur regroupe six départements issus des
provinces de l'Ancien Régime de Provence et du Dauphiné:

- Alpes-de-Haute-Provence (04)
- Hautes-Alpes (05)
- Alpes-Maritimes (06)
- Bouches-du-Rhône (13)
- Var (83)
- Vaucluse (84)

Unions départementales Provence-Alpes-Côte d'Azur
==================================================

.. toctree::
   :maxdepth: 4

   06/index
   13/13
   83/index
   84/index
