
.. index::
   pair: Film; Disparaissez les ouvriers

.. _activites_stics13_2012:

=======================================
Activites stics13 2012
=======================================


21 janvier 2012 "Au prix du Gaz - Silence des machines, paroles d’ouvriers"
============================================================================

Projection à Marseille, samedi 21/01/12.


.. seealso:: http://www.cnt-f.org/spip.php?article1818



29 septembre 2012 "Disparaissez les ouvriers"
==============================================

::

	Sujet: 	[Liste-syndicats] fete de la CNT 13
	Date : 	Thu, 6 Sep 2012 12:48:17 +0200
	De : 	"CNT - stics Bouches du Rhône" <stics.13@cnt-f.org>
	Pour : 	liste synd <liste-syndicats@bc.cnt-fr.org>


Salut,
une petite annonce de la CNT 13 qui fête la rentrée

Samedi 29 septembre à l'équitable café (http://equitablecafe.org/)
54 cours Julien (6eme)

avec projection de "Disparaissez les ouvriers" et débat autour de la
dégradation des conditions de travail (entre autre) en présence des
réalisateur-trice,

repas préparé par la Kuzin, association autogérée marseillaise
et musique avec "La lutte enchantée" chorale, le "Vers noir" chansons
libertaires et Tornamai le bal qui débogue.

Nous espérant nombreux, avec nos saluts fraternels
MpI secrétariat du Stics 13
