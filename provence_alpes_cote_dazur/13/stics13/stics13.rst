
.. index::
   pair: syndicat; STICS13
   pair: STICS; 13
   ! STICS13


.. _stics_13_pub:
.. _stics_13:
.. _stics13:

==========================================================================================================
Syndicat des travailleurs de l’Industrie et du  Commerce et des Services  de Marseille (STICS13,Marseille)
==========================================================================================================

.. figure:: stics13.png
   :align: center

   *STICS 13*

.. seealso::

   - :ref:`ud13_publique`
   - http://www.cnt-f.org/sam/
   - http://www.cnt-f.org/sam/spip.php?rubrique17




Coordonnées
===========

:Adresse courriel:  stics.13@cnt-f.org


::

    Sujet:  [Liste-syndicats] changement d'adresse du Stics 13
    Date :  Tue, 16 Apr 2013 12:36:05 +0200
    De :    "CNT - stics Bouches du Rhône" <stics.13@cnt-f.org>


Bonjour,
pour info, nous, le Stics 13 avons donc déménagé et notre nouvelle
adresse est::

    CNT Stics 13
    c/o 1000 Bâbords
    61, rue Consolat
    13001 Marseille


avec nos fraternelles salutations
MpI pour le Stics 13

Permanence à 1000 Bâbords les 3eme samedi du mois, 14h00/16h00

Activités
=========

.. toctree::
   :maxdepth: 4

   activites/index


Motions
========

.. toctree::
   :maxdepth: 4

   motions_congres/motions_congres
