
.. index::
   pair: Union Locale; Marseille
   ! Marseille


.. _ul_marseille_pub:

=======================================================================================
Union locale Marseille (UL33)
=======================================================================================

.. seealso::

   - :ref:`ud13_publique`


.. figure:: cnt_marseille.png

   *CNT Marseille*


Adresse
=======
::

	Permanences le 1er et 3ème samedi du mois de 14h à 16h
	12, rue de l’Évêché,
	13002 Marseille
    téléphone : 06 01 10 50 40
