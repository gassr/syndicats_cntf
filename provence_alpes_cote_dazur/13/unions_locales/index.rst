
.. index::
   pair: syndicats; CNT Bouches du Rhône (13)
   pair: Union départementale; UD13
   ! Bouches du Rhône


.. _unions_locales_13_publique:

=======================================
Unions Locales Bouches du Rhône
=======================================


Marseille
==========

.. toctree::
   :maxdepth: 4

   marseille/index
