
.. index::
   pair: syndicats; CNT Haute-Normandie
   pair: Union Régionale ; Haute-Normandie
   ! Haute-Normandie


.. _union_regionale_haute_normandie:

=======================================================
Union Régionale Haute-Normandie
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Haute-Normandie


:code région: 23


La Haute-Normandie, en référence à la latitude, mais d'altitude plus basse que
la Basse-Normandie, est une région de France créée en 1956 qui regroupe deux
départements :

- Eure (27)
- Seine-Maritime (76)

Elle correspond à la partie orientale de l'ancienne province de Normandie.


.. toctree::
   :maxdepth: 4

   76/index
