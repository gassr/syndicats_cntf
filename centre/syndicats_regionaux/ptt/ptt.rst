
.. index::
   pair: Syndicat régionale ; PTT Centre


.. _ptt_centre:

=====================================================================
PTT Centre
=====================================================================

.. seealso::

   - :ref:`union_regionale_centre`

Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres
