

.. index::
   pair: Réunification ; CNT


.. _appel_reunification_CNT:

===================================================
Appel pour une réunification/refondation des CNT
===================================================

.. contents::
  :depth: 3


Motifs
========

Constat
---------

La Confédération Nationale du Travail, en tant que Confédération de
Syndicats anarcho-syndicalistes et syndicalistes révolutionnaires, s’est
déchirée et affaiblie depuis au moins une trentaine d’années sur des
différents internes.

Ces désaccords ont abouti à chaque fois:

- à une scission confédérale et l'existence à ce jour d'au moins 3
  organisations syndicales se réclamant de*la CNT*: (CNT-F dite Vignoles,
  CNT-SO, CNT-AIT)
- au départ de militants souvent anciens, vers d'autres organisations
  syndicales que les 3 CNT listées, ou vers nulle part et cela après des
  déclarations publiques et assumées ou bien dans le silence le plus complet.

Analyse
---------

Il y aurait beaucoup d'intérêt et de bénéfice pour chacun à évaluer ce qui
nous a séparé de ce qui nous a réuni.

En préalable, on peut légitimement poser que les différences qui nous ont
séparées restent bien minimes en regard de ce qui pouvait nous rassembler
surtout si on veut bien considérer le combat commun que nous menons contre
nos véritables adversaires: le Capitalisme, une société de classes et la
nécessité d'une révolution sociale et de luttes sociales dans lesquelles
nous sommes tous engagés.

On peut aussi envisager que ces différents auraient pu et surtout peuvent
encore être solutionnés par l'application d'un mode de fonctionnement auquel
nous devrions tous être attachés: le Fédéralisme et son corollaire:
l'autonomie des syndicats.

Le Fédéralisme, à notre sens,*oblige* à reconnaître:

- la reconnaissance que la CNT est une Confédération de syndicats qui y
  adhérent et non la juxtaposition d'individus qui y adhérent individuellement.
- la reconnaissance d'un socle commun: les statuts confédéraux
- l’autonomie des syndicats
- la reconnaissance des décisions de Congrès (motions adoptées majoritairement)
  sans que soit remis en question le droit pour chaque syndicat affilié de ne pas
  les appliquer pour lui même et seulement pour lui même et sans que ce syndicat
  puisse nuire à l'application de ces décisions majoritaires par ceux et
  pour ceux qui en ont décidé.

Ces quatre principes posés et actés on aboutirait à un véritable**pacte confédéral**
posé, écrit et circonscrit sans que cette utilisation du terme de**pacte confédéral**
ne soit utilisé par tous et chacun dans le flou et l’incertitude le plus complet.

Il serait long d’égrener toutes les différentes positions qui ont abouti dans
le passé à nos différentes scissions et départs de militants.

Revenons sur les principales:

- mise en avant de la nécessité d'axer notre lutte syndicale dans l'entreprise
  et dans le champ professionnel tout en reconnaissant la nécessité de la lutte
  interprofessionnelle et les combats*sociétaux* VERSUS mise en avant de la
  primauté de l'action syndicale hors champ professionnel en privilégiant
  les syndicats Intercorporatifs et les luttes sur le terrain sociétal.
  (scission avec la CNT-AIT)
- Possibilité pour les syndicats CNT de se présenter (dans un cadre établi) à
  certaines élections professionnelles VERSUS refus de la participation des
  syndicats à de tels scrutins comme*inhérent* à une vision de l'identité
  de la Confédération. (scission avec la CNT-AIT)
- Refus de permettre aux syndicats de faire appel à des salariés adhérents
  pour prendre en charge l'activité de défense prud’hommale des salariés
  VERSUS nécessité présentée par certains syndicats de faire appel à de tels
  salariés dans ce champ précis d'activité syndicale
  (scission avec la CNT Solidarité Ouvrière).

Concernant ce point il est aisé de voir qu’il aurait été plus judicieux de
mettre en application le Fédéralisme (non utilisation de salariés dans ce
champ précis d'action syndicale par les syndicats qui le souhaitaient
majoritairement et utilisation pour ceux qui le souhaitaient de le
faire).

On notera que les uns et les autres font appel aux services rémunérés
de la profession libérale que constitue l'ordre des avocats.

Est-il faux de considérer que c’est DEPUIS cette scission sur ce point
particulier que de nombreux syndicats adhérents à la CNT-F ont véritablement
investi avec pugnacité et efficacité la défense individuelles des salariés
tout en ne faisant pas appel à des adhérents salariés ?

La confrontation et la mise en commun au sein de la même Confédération de ces
deux expériences pratiques auraient pu et permettraient encore de faire avancer
tout le monde sur cette activité précise.

A ces scissions passée et*historiques* on pourrait ajouter le spectre possible
pour la CNT-F de possibles futures scissions (si ce mode de fonctionnement de
type*centraliste démocratique* perdure en délaissant le fédéralisme et
l'autonomie des syndicats).
Il existe en notre sein des divergences d'appréciation (voir de*doctrine*)
sur les analyses du*Combat Féministe*, c’est un intitulé par défaut mais il
est surement réducteur, les diffèrentes approches du Féminisme et les pratiques
qui en découlent.

Verrons nous ces différences aboutir à la création d’une prochaine CNT
*Féministe* VERSUS une CNT-F (maintenue) mais non pas anti-féministe ?

Encore une fois la mise en exercice du fédéralisme et de l'autonomie des
syndicats ne permettraient-ils pas aux syndicats opposés sur ce sujet de
développer leurs analyses et leurs pratiques - de les échanger, de les confronter -
sans que l'une ou l'autre des parties prenantes veuillent en faire
un*dogme majoritaire*?

Dans un même ordre d’idée et opportunité de déchirement des quels nous sommes
coutumiers, il n’est pas faux de dire qu’il existe des différences d'approche
sur le thème: de l'antifascisme ou de la nécessité de se développer en
avant-garde (ou*en tête de manif*) VERSUS l'affirmation que nous avançons
tous ensemble et que le rôle des avant garde est de partir loin devant et
quelques fois si loin qu'en se retournant elles prennent le risque de se
retrouver très seules.

L'application du fédéralisme et de l'autonomie des syndicats est une possibilité
de régler ce type de différences d'approche.

Enfin, il apparait nettement que certains syndicats (ou adhérents mais là
c’est leur droit) veulent faire rentrer la CNT dans le giron des
*organisations anarchistes* en l'ajoutant simplement à la longue liste
des organisations politiques libertaires affinitaires présentes*sur le marché*
ou du moins voient d’un bon oeil que la Confédération puisse être qualifiée
de*syndicat anarchiste*.

Ce serait là l'abandon totale de la spécificité de notre projet confédéral, de
notre indépendance organisationnelle, de notre mode d'organisation.

Oui, la Confédération est une organisation de syndicats anarcho-syndicalistes,
syndicalistes révolutionnaires,

Oui, elle doit beaucoup historiquement aux apports (parmi d'autres) des
anarchistes,

Oui, elle est elle même libertaire en tant que non autoritaire et qu’elle pose
la nécessité du remplacement de l'Etat par un mode de fonctionnement de
la société: le communisme libertaire.

Non, la Confédération ne se réduit pas à être une autre et une de plus
organisation anarchiste

Par contre il y a bien un pendant à cette volonté d’indépendance,
d’originalité et d’autonomie par rapport aux organisations spécifiques
c’est que la*CNT* n’interdit pas (elle n’a même pas à autoriser puisque
c’est hors de son champ) l'affiliation et la militance individuelle de tel
ou de tel des adhérents de ses syndicats à une organisation spécifique.
L’important là n'étant pas d'éviter certaines contradictions possibles mais
simplement la confusion des champs d’adhésion, de militance et de
responsabilité.

Il reste nécessaire que cette adhésion et militance individuelle ne soit pas
revendiquée comme moyen de faire adhérer ou d'inféoder son syndicat
d'appartenance à une quelconque organisation politique spécifique.

Enfin, aucun syndicat CNT, en tant que tel ne peut être adhérent et affilié
à une quelconque organisation politique spécifique.

Dans l'application de son principe d'autonomie La Confédération est souveraine
et s’autorise toutes les actions unitaires et communes avec les organisations
qu'elle souhaite, spécifiques ou syndicales dès lors que ces actions unitaires
et communes ont été approuvées par la majorité des syndicats qui la compose.

De même chaque syndicat, peut s’autoriser et est souverain pour engager et
mettre en pratique de telles actions avec de telles organisations même si cela
n’a pas été validé au niveau confédéral.
Il en va de même pour les Fédération d'Industrie de la CNT qui jouissent
du même principe d'autonomie.

Conclusion (en guise de)
=========================

On pourra objecter (et certains le feront sans doute) à un tel projet de
*pacte confédéral* d'être un extraordinaire fourre tout qui conduirait à:

- tenter de réunir des positions*supposées* définitivement antagonistes,
- de dénaturer l’une ou l’autre approche de l’*identité* de la Confédération,
  et de permettre un flou improductif et déroutant,
- de permettre ou d’introduire un*droit de tendances constituées* qui n’a
  jamais été reconnu par la Confédération.

A cette dernière objection putative on pourra répondre que: le Fédéralisme
et l'autonomie des syndicats sont difficiles à mettre en oeuvre mais qu’ils
nous sont rendu obligés si nous ne voulons pas tomber (ou rester) dans un
fonctionnement de type*centraliste démocratique même*libertaire**
qui ne reconnait que le fait majoritaire et qui aboutit à l'exclusion ou
aux départs de toutes les structures (ou individus adhérents aux syndicats)
dès lors qu’ils ne se reconnaissent plus dans le fait majoritaire.

Pour autant l'application du Fédéralisme et la possibilité de l’existence
de*tendances constituées* à l'intérieur de la Confédération ne sont
pas de même nature et n'aboutissent pas aux mêmes effets.

Le Fédéralisme permet la diversité cohérente et l'autonomie des syndicats
ALORS QUE le droit de tendances constituées en tant que telles aboutit à
des prises de position à l'adoption ou le refus d’orientations non pas en
fonction de l'adhésion ou du refus sur le fond de ces propositions mais en
fonction de l'adhésion de tel ou tel syndicat à telle ou telle tendance
constituée, dénaturant ainsi complètement l’intérêt spécifique de ces
propositions.

Ce type de fonctionnement aboutirait à faire de la Confédération un véritable
champ de bataille interne et une aubaine pour toutes les luttes d’influence
et de pouvoir qui pourraient y voir le jour.

On pourra aussi, tout en reconnaissant qu'il puisse être amélioré, prendre
position pour ce projet de*pacte confédéral*.

C’est ce que nous proposons dès aujourd’hui:

- aux syndicats de la Confédération Nationale du Travail dite CNT-F, à ses
  Fédérations de syndicats, à leurs adhérents
- aux syndicats de la Confédération Nationale du Travail dite CNT-SO, à ses
  Fédérations de syndicats, à leurs adhérents
- aux syndicats de la Confédération Nationale du Travail dite CNT-AIT,
  à ses Fédérations de syndicats, à leurs adhérents

Ainsi qu’aux militants ayant adhéré et milité dans le passé dans ces
Confédérations.

Tous ceux qui voudraient soutenir un tel projet ne sont tenus à le faire que
sur le texte de l'appel lui même et non pas sur les Motifs:

- Constat/Analyse/ Conclusions présentés en avant propos.

Les réponses sont à faire parvenir à::

    Syndicat CNT-PTT
    Région Centre: 9 rue des Rosiers,
    37150 Epeigné les Bois
    e-mail: sergemor@wanadoo.fr


Publicité
==========

Dans un premier temps nous soumettrons cet appel à tous les syndicats de notre
confédération l'envoi de cet appel sera donc fait: par son envoi et sa
publication sur la Liste Confédérale des Syndicats, sur la Liste fédérale
des syndicats CNT-PTT, sur le Bulletin Intérieur de la Confédération, par des
envois papiers aux adresses des syndicats.

Nous demanderons sa publication au Combat Syndicaliste en laissant un laps de
temps nécessaire pour que les différents syndicats de notre Confédération
aient pu en prendre connaissance et éventuellemnt en débattre.

Chaque syndicat pourra y souscrire:

- en le reprenant à son compte
- en l'état
- y proposer des modifications (si elles ne sont pas de nature à en changer
  complètement le sens)
- le refuser en bloc
- l'ignorer

Après cette période de maturation, et seulement après, il faudra bien le
proposer aux autres syndicats affiliés aux autres CNT (SO, AIT) puisqu'ils
peuvent en être partie prenante la publicité sera faite alors par:

- en ce qui concerne la CNT-SO: par son envoi à son adresse et à celles de
  ses syndicats
- en ce qui concerne la CNT-AIT: par son envoi à son adresse et à celles de
  ses syndicats
- en ce qui concerne les anciens adhérents ou militants à la CNT-F par l'envoi
  aux adresse personnelles dans la limite des connaissances que nous en avons.

Enfin, il n’est pas interdit aux structures syndicales et à leurs adhérents de
faire connaître par leurs propres moyens le contenu d’un tel projet.

En tout état de cause, notre appel, pourra être repris par notre syndicat sous
forme de motion, à notre prochain Congrès Confédéral.
De la même manière, ayant conscience de notre effectif limité nous appellerions
les syndicats qui le souhaitent à reprendre cette motion à leur compte et d’en
devenir*co-porteurs*.

Appel pour une réunification/refondation des CNT (le texte)
============================================================

La Confédération Nationale du Travail est une Confédération de syndicats qui y
adhérent et non la juxtaposition d'individus qui y adhérent individuellement.

- les syndicats qui s'y affilient adhérent aux statuts confédéraux.
- chaque syndicat qui s’y affilie la Confédération Nationale du Travail est
  régit par ses propres statuts pour ce qui lui est propre sans qu’il remettent
  en cause les statuts confédéraux pour ce qui concerne la Confédération
- la Confédération Nationale du Travail reconnait l’autonomie des syndicats et
  des Fédérations de syndicats qui lui sont affiliés.
- la Confédération Nationale du Travail reconnait les décisions de Congrès
  (motions adoptées majoritairement) sans que soit remis en question le droit
  pour chaque syndicat affilié de ne pas les appliquer pour lui même et seulement
  pour lui même et sans que ce syndicat puisse nuire à l'application de ces
  décisions majoritaires par ceux et pour ceux qui en ont décidé.
  (application du Fédéralisme en opposition un*centralisme démocratique*)
- la Confédération Nationale du Travail est une confédération de syndicats
  anarcho syndicalistes et /ou syndicalistes révolutionnaires.
  Elle revendique son projet syndical propre et original en complète
  indépendance de toutes organisations politiques spécifiques quelle qu’elle
  soit.
- la Confédération Nationale du Travail s’inscrit son combat dans la nécessité
  de mener à bien la lutte de classes, le combat anticapitaliste et contre les
  autres formes de domination, l’internationalisme et la fraternité entre les
  peuples. Son projet original est un combat pour une société sans classe,
  sans Etat, débarrassée de toutes les dominations et exploitations, l'avènement
  d'une pleine justice sociale, son moyen est le Communisme Libertaire.
- la Confédération Nationale du Travail n’a pas à légiférer sur les adhésions
  individuelles des adhérents de ses syndicats à telle ou telle organisation
  spécifique politique.
  En revanche, la Confédération Nationale du Travail, réaffirmant son
  indépendance, n’admet pas l'affiliation de l’un de ses syndicats en tant
  que tel à une quelconque organisation politique affinitaire quelle qu'elle
  soit.
