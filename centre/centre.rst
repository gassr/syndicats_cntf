
.. index::
   pair: syndicats; CNT Centre
   pair: Union Régionale ; Centre
   ! Centre


.. _union_regionale_centre:

=======================================================
Union Régionale Centre
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Centre


.. figure:: centre.png

   *La région centre*


:code région: 24


Six départements composent la région Centre, regroupant 185 cantons et 1842
communes pour une superficie de 39 151 km2:


- Cher (18)
- Eure-et-Loir (28)
- Indre (36)
- Indre-et-Loire (37)
- Loir-et-Cher (41)
- Loiret (45)

Les syndicats régionaux du Limousin
====================================

.. toctree::
   :maxdepth: 4

   syndicats_regionaux/syndicats_regionaux
