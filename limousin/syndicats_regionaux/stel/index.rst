
.. index::
   pair: syndicat régional; STE Limousin



.. _stel:

============================================================
Syndicat Travailleurs_euses de l'Education Limousin
============================================================

Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/index
