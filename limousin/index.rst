
.. index::
   pair: syndicats; CNT Limousin
   pair: Union Régionale ; Limousin
   ! Limousin


.. _union_regionale_limousin:

=======================================================
Union Régionale Limousin
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Limousin


.. figure:: limousin.png

   *La région Limousin*



:code région: 74

- Corrèze (19)
- Creuse (23)
- Haute-Vienne (87)

Les Unions départementales de la région Limousin
=======================================================


.. toctree::
   :maxdepth: 4

   87/index

Les syndicats régionaux du Limousin
====================================

.. toctree::
   :maxdepth: 4

   syndicats_regionaux/index
