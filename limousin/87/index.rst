
.. index::
   pair: syndicats; 87
   pair: Union départementale; UD87
   ! Haute_Vienne
   ! 87



.. _ud87_publique:

=========================================
Union départementale Haute-Vienne (UD87)
=========================================

.. seealso::

   - :ref:`union_regionale_limousin`
   - http://cnt87.org


::

	Union Locale Limoges
	6 rue de Gorre
	87000 Limoges
	Mail : cnt87@cnt-f.org
	Web : http://cnt87.org
