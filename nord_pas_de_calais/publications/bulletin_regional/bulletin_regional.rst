
.. index::
   pair: Bulletin régional; Nord Pas-de-Calais


.. _bulletin_regional_nord_pas_de_calais:

====================================
Bulletin régional Nord Pas-de-Calais
====================================

.. seealso::

   - http://www.cnt-f.org/59-62

.. contents::
   :local:


Abonnement
==========


Pour se procurer où s’abonner au bulletin régional de la CNT 59/62 :

Achat au numéro : 1 euro (2 euros par courrier postal)

- Abonnement ordinaire : 6 euros pour quatre numéros.
- Abonnement de soutien : à vot’ bon coeur !
- Règlement à l’ordre de l’union régionale CNT 59/62 (mention « Bulletin régional » au dos).

Adresse
=======

::

    UL-CNT, 32 rue d’ Arras
    59000 LILLE
    Courriel : ul-lille@cnt-f.org

Prochain numéro
===============

Le numéro 31 du bulletin paraîtra pour l’été 2012.

Envoyez-nous au plus tôt vos propositions d’articles et d’interviews, vos notes
de lecture, vos illustrations, etc. à ur59-62@cnt-f.org


Liste des numéros
=================


.. toctree::
   :maxdepth: 4

   2012/2012
