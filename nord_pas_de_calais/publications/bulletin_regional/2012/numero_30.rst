

.. figure:: ../repartition_egalitaire_des_richesses_59_62.jpg
   :align: center

   **Répartition égalitaire des richesses !**



.. _bulletin_regional_nord_pas_de_calais_2012_30:

===============================================
Bulletin régional Nord Pas-de-Calais Numéro 30
===============================================


Le bulletin régional n° 30 de la CNT 59/62 est sorti.

Sommaire
=========

- 02 Un petit Quizz
- 03 Quoi de neuf à la CNT ?
- 04 Billet d’humeur !
- 05 La confédération Nigérienne du Travail
- 06 La faim du Monde
- 08 Contre la vie chère - Benoît Broutchoux
- 09 Conférences « Éduquer pour émanciper »
- 10 Regard international
- 11 Manifestation anti-fasciste à Lille (oct. 2011)
- 12 Organigramme de l’extrême droite
- 14 Dossier **2006-2010 : continuité et divergences des résistances sociales**
    Le mouvement contre la réforme des retraites de 2010
    De la lutte anti-CPE au mouvement contre la réforme des retraites
- 20 Entretien avec José Faran
- 21 Lectures du chat noir
- 22 Contacter la CNT
- 23 La CNT, c’est quoi ?
- 24 Campagne de la CNT pour la répartition des richesses
