
.. index::
   pair: STIS59; sections syndicales


.. _sections_syndicales_stis59:

=====================================================
Sections syndicales du syndicat CNT STIS59
=====================================================

.. toctree::
   :maxdepth: 4

   sevelnord/sevelnord
   shenker_joyau/shenker_joyau
