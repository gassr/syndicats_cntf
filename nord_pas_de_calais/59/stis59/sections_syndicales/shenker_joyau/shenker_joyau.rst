
.. index::
   pair: STIS59; Schenker Joyau 59


.. _sections_syndicales_shenker_joyau59:

==============================================
Section syndicale Shenker Joyau Nord
==============================================

.. seealso::

   - :ref:`syndicat_entreprise_shenker_joyau_nord`
   - http://www.cnt-f.org/59-62/2011/12/creation-de-la-federation-cnt-des-travailleurs-des-transports-de-la-logistique-et-activites-auxiliaires/
