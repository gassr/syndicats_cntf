.. index::
   pair: Syndicat; STIS59
   ! STIS59


.. _stis59_pub:

==============================================================================
Syndicat CNT des Travailleurs de l’Industrie et des Services du Nord (STIS59)
==============================================================================

.. seealso:: ..

   - :ref:`ud59_publique`
   - http://www.cnt-f.org/spip.php?article24

Syndicat CNT des Travailleurs de l’Industrie et des Services du Nord (STIS 59).

:Adresse mail: stis59@cnt-f.org

.. toctree::
   :maxdepth: 4

   activites/activites
   sections_syndicales/sections_syndicales
