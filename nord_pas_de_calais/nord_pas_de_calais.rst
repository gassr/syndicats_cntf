
.. index::
   pair: Union régionale ; Nord Pas-de-Calais
   pair: Liste de diffusion; Nord Pas-de-Calais
   ! Nord Pas-de-Calais


.. _union_regionale_nord_pas_de_calais:

====================================
Union Régionale Nord Pas-de-Calais
====================================

.. seealso::

   - http://www.cnt-f.org/59-62
   - https://fr.wikipedia.org/wiki/Nord-Pas-de-Calais


:code région: 31


Le Nord-Pas-de-Calais est une région administrative française,
administrée par un Conseil régional, assisté du CESER ; le Conseil
régional du Nord-Pas-de-Calais et une préfecture de région, tous deux
basés à Lille.

Cette région, autrefois minière, encore très industrialisée, et cultivée
sur 75 % de son territoire est bordée au sud par la région Picardie,
de la Manche et la mer du Nord à l'ouest et au nord-ouest, avec le
Royaume-Uni et par la Belgique au nord-est.

Elle est composée de deux départements:

- Nord (59)
- Pas-de-Calais (62)


Avec 326 hab.km-2, elle compte parmi les régions d'Europe les plus
densément peuplées.

.. contents::
   :local:

Adresse
=======

.. .. seealso:: http://www.cnt-f.org/59-62


- Adresse : UR-CNT, 32 rue d’Arras, 59000 Lille
- Tél : 03 20 56 96 10
- Permanences :

  - le mardi de 18 à 19h,
  - le jeudi de 17h30 à 19h30,
  - le samedi de 14 à 15h

:courriel: ur59-62@cnt-f.org
:courriel: ul-lille@cnt-f.org
:courriel: stt59@cnt-f.org


Liste de diffusion
==================

.. seealso:: https://listes.globenet.org/listinfo/liste-cnt-infos-nord

Lettre d'information de l'union régionale interprofessionnelle CNT du Nord
Pas-de-Calais, de ses syndicats, sections syndicales et unions locales.


Publications
============

.. toctree::
   :maxdepth: 4

   publications/publications


Unions Départementales Nord Pas-de-Calais
=========================================


.. toctree::
   :maxdepth: 4

   59/59
   62/62
   syndicats_regionaux/syndicats_regionaux
