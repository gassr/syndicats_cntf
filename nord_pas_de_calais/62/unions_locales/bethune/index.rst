
.. index::
   pair: Union Locale ; Béthune
   ! Béthune



.. _ul_bethune_publique:

=========================================
Union Locale Béthune (UD62)
=========================================

.. seealso::

   - http://www.cnt-f.org/59-62/2012/01/un-nouveau-local-pour-lul-cnt-de-bethune/
   - :ref:`ud62_publique`

Adresse
=======

	CNT
	Maison des syndicats - Centre Jean Monnet II - entrée B
	6 place de l’Europe
	62400 BÉTHUNE

C'est dans la ZUP du Mont Liébaut, au bout de l'avenue du mont Liébaut et de
l'avenue de Rome (4ème étage, dont 3 avec ascenseur)

Permanences
===========

Permanence tous les vendredi de 18 à 19h
