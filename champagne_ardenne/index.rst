
.. index::
   pair: syndicats; CNT Champagne-Ardenne
   pair: Union Régionale ; Champagne-Ardenne
   ! Champagne-Ardenne


.. _union_regionale_champagne_ardenne:

=======================================================
Union Régionale Champagne-Ardenne
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Champagne-Ardenne


.. figure:: champagne_ardenne.png

   *La région Champagne-Ardenne*


:code région: 21


La région Champagne-Ardenne se situe au Nord-Est de la France.

Elle possède une frontière commune avec la Région wallonne (Belgique). Elle est
composée de 4 départements (les Ardennes, la Marne, l’Aube et la Haute-Marne).

Enfin, elle s'étend sur une superficie de 25 606 km2.

- Ardennes (08)
- Aube (10)
- Marne (51)
- Haute-Marne (52)
