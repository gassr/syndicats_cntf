
.. index::
   pair: Syndicat; interco21
   ! interco21
   ! 21

.. _interco21:

==============================================================================
Syndicat **intercorporatif de la Côte d’Or (interco21)**
==============================================================================


::

    http://www.cnt-f.org/_cnt-interco-21_.html



Introduction
=============

Adresse
==========

::

    CNT 21
    6b rue Musette
    21000 Dijon

Vous pouvez inscrire à la liste publique : https://listes.globenet.org/listinfo/la.craie.noire-diffusion
ou nous écrire à : interco.21@cnt-f.org
