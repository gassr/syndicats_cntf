

.. _educ21:

==============================================================================
**Educ 21**
==============================================================================


::

    http://www.cnt-f.org/_cnt-interco-21_.html




Adresse
==========

::

    CNT 21
    6b rue Musette
    21000 Dijon

Vous pouvez inscrire à la liste publique : https://listes.globenet.org/listinfo/la.craie.noire-diffusion
ou nous écrire à : interco.21@cnt-f.org



Motions
========

.. toctree::
   :maxdepth: 3

   motions/motions
