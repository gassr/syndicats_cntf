
.. index::
   pair: syndicats; CNT Bourgogne
   pair: Union Régionale ; Bourgogne
   ! Bourgogne


.. _union_regionale_bourgogne:

=======================================================
Union Régionale Bourgogne
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Bourgogne_(ancienne_r%C3%A9gion_administrative)
   - http://www.cnt-f.org/spip.php?article35


.. figure:: bourgogne.png

   *La Bourgogne*


:code région: 26


Elle compte 4 départements:


- Côte-d'Or (21)
- Nièvre (58)
- Saône-et-Loire (71)
- Yonne (89)


.. toctree::
   :maxdepth: 6

   21/21
   58/58
   71/71
