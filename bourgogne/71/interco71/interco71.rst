
.. index::
   pair: Syndicat; interco71
   ! interco71
   ! 71


.. _interco71_pub:
.. _interco71:

==============================================================================
Syndicat **intercorporatif CNT Saône-et-Loire (interco71)**
==============================================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Sa%C3%B4ne-et-Loire
   - :ref:`ud71_publique`


.. seealso:: https://fr.wikipedia.org/wiki/Cuisery

.. figure:: cuisery.png

   *Le village de Cuisery*


.. figure:: cuisery_carte.png
   :align: center

   https://fr.wikipedia.org/wiki/Cuisery





Introduction
=============

Cuisery est une commune française, située dans le département de
Saône-et-Loire et la région Bourgogne. Ses habitants s'appellent
les Cuiserotains.


Adresse Interco71
==================

::

    Syndicat Intercorporatif  CNT de Saône et Loire
    19 rue du pavé
    71290 CUISERY
    tel : 06 01 22 17 94



:Adresse courriel: interco71@cnt-f.org
:Roger Chambart: chats.noirs@yahoo.fr


Actions Interco71
==================

.. toctree::
   :maxdepth: 4

   actions/actions


Livre libertaire
=================

.. toctree::
   :maxdepth: 6

   livre_libertaire/livre_libertaire


Luttes Saône-et-Loire (71)
==========================

.. toctree::
   :maxdepth: 4

   luttes/index

Motions Congrès Interco71
==========================

.. toctree::
   :maxdepth: 4

   motions_congres/motions_congres


Tracts
==========================

.. toctree::
   :maxdepth: 4

   tracts/tracts
