.. index::
   pair: Interco71 ; 2014


.. _actions_interco_71_2014:

===========================
Actions de l'Interco71 2014
===========================


::

    Sujet :     [Liste-syndicats] [Fwd: Tr : Vidéos en liberté]
    Date :  Thu, 2 Oct 2014 20:17:28 +0200
    De :    Syndicat CNT Interco71 <interco71@cnt-f.org>
    Pour :  liste-syndicats@bc.cnt-fr.org


Bonjour a toutes et à tous,

Le syndicat CNT 71 sera présent lors des Videos en liberté  organisés par
Videos en liberté et la librairie les chats noirs.
les 11 et 12 octobre 2014 conférences et films sur les fusillés de la guerre 14/18.


Merci de passer l'information
Salutation des chats noirs
