.. index::
   pair: Autogestion; Intreco 71 (2020-03-14)

.. _autogestion_en_acte_2020_03_14:

========================================================================================================================
Samedi 14 mars 2020 : soirée PUBLIQUE pour l'autogestion **Espagne libertaire des années 30, l'autogestion en acte**
========================================================================================================================

::

    Objet:   [Liste-syndicats] conference debat
    De:      "Syndicat CNT Interco 71-58" <interco71@cnt-f.org>
    Date:    Mer 12 février 2020 10:16

    Bonjour,

    ci joint l'affiche de notre prochaine soirée PUBLIQUE pour l'autogestion

    Roger pour la CNT71/58


.. figure:: interco71_autogestion_2020.png
   :align: center


Film **Le reve égalitaire dans l'Espagne libertaire**, suivi d'une
conférence animée par Myrtille, giménologue.

Soirée pour l'autogestion organisée par le syndicat CNT de Saone et Loire.
