
.. _admr_71_2013:

==============================
Lutte Aide a domicile 71 2013
==============================



Les salariées de l'aide et du maintien à domicile affirment :
leur droit de vivre de leur salaire, et non de survivre !

- Salaires bloqués au SMIC pendant 13 ans.
- Utilisation de nos véhicules personnels
  sans prise en compte réelle de nos frais de
  déplacements.
- Non-respect des qualifications.
- Réductions des congés d'ancienneté.


Nous ne pouvons accepter que les choses restent en l'état, nous
refusons le nivellement par le bas.

Le maintien à domicile, la perte de l'autonomie, le handicap sont en
constante progression.

Nos professions sont incontournables et leur utilité publique
n’est plus à démontrer !

Ca ne peut plus durer.

NOUS SOMMES TOUTES ET TOUS CONCERNÈS !!!
