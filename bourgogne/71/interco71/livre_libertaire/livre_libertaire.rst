.. index::
   pair: Salon du livre libertaire ; Roger Chambard
   pair: Salon du livre libertaire ; Interco71
   pair: Salon du livre libertaire ; Cuisery


.. _livre_libertaire:

===========================
Salon du livre libertaire
===========================




.. seealso::

   - http://cuisery-villagedulivre.com/les-boutiques/les-chats-noirs-2/ (nouveau site)
   - http://cuisery.livre.free.fr/spip.php?rubrique72 (ancien site)


.. figure:: chats_noirs.jpg
   :align: center

   *Librairie Les Chats Noirs*




Adresse
========

::

    19, rue du pavé
    71290 Cuisery

    Tél: 03.85.40.05.65

Spécialité : Histoire des mouvements sociaux, écologie, mondialisation,
mouvement libertaire.

Ouverture : Week-end et jours fériés ainsi qu’au gré des envies...

Roger Chambard
==============

- http://atelierdecreationlibertaire.com/blogs/images-de-libertaires/1987-%c2%ab-a-montrochet-les-schismes-passent-les-anars-restent-%c2%bb-1444/
- http://atelierdecreationlibertaire.com/blogs/images-de-libertaires/tag/cnt/


:Tél: 03.85.40.05.65

Village du Livre
================

.. seealso::

   - http://cuisery-villagedulivre.com/ (nouveau site)
   - http://cuisery.livre.free.fr/ (ancien site)
   - http://www.cuisery.fr/associations-culturelles.htm


Compte twitter
--------------

- https://twitter.com/CuiseryVDL

Vidéo
-----

.. seealso:: http://www.youtube.com/watch?feature=player_embedded&v=MKjhGqIeWBo


Editions
==========


.. toctree::
   :maxdepth: 3

   2020/2020
   2018/2018
   2012/2012
