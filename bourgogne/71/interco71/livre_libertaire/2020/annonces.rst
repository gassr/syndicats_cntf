

===========================
Annonces
===========================




La Feuille d’infos du CIRA, n° 230, août-septembre 2020
=========================================================

.. seealso::

   - http://www.millebabords.org/spip.php?article34609

CLUNY (SAÔNE-ET-LOIRE) : dimanche 27 septembre 2020 de 10 heures à 18 heures.

Le groupe libertaire de Saône-et- Loire et les syndicats CNT 71 organisent
la 12e édition du Salon du livre libertaire en Saône-et-Loire.

Le thème choisi pour cette rencontre est : **Le municipalisme libertaire**.

Il y aura une restauration à prix libre, une buvette, des débats (dès le samedi soir)
et un apéritif festif.

Adresse : Espace des Griottons, 71250 Cluny (courriel : jeanluc.coedic chez free.fr).

Salon-du-livre-libertaire
==============================

.. seealso::

   - https://www.lejsl.com/pour-sortir/loisirs/Rencontre-conference/Conferences/Bourgogne/Saone-et-loire/Cluny/2020/09/26/Salon-du-livre-libertaire

Salon du livre libertaire
Quand, Où ?

- le 26/09/2020 à 20h00
- le 27/09/2020 à 10h00, à 19h00

Les Griottons
rue des Griottons
Cluny

Organisateur

- Syndicat CNT 71
  06.01.22.17.94
