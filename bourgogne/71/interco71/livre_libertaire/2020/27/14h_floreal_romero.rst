
.. _floreal_romero_2020:

===========================================================================
14h Présentation du livre de **Floréal Roméro** sur l'écologie sociale
===========================================================================

.. seealso::

  - https://www.editionsducommun.org/products/agir-ici-et-maintenant-floreal-m-romero


.. contents::
  :depth: 3

**Floréal M. Romero**
=======================

**Floréal M. Romero** est issu de la tradition anarchosyndicaliste
espagnole par son père.

Il adhère aux thèses de Bookchin et en devient un des principaux promoteurs
en Espagne, mais aussi en France à travers des rencontres, des publications
et des articles.
Il vit en Andalousie où il est producteur d’avocats et travaille uniquement
en lien avec des Associations pour le maintien d’une agriculture paysanne (AMAP).


Agir ici et maintenant Penser l’écologie sociale de **Murray Bookchin**
=========================================================================

.. figure:: agir_ici_et_maintenant.png
  :align: center

  https://www.editionsducommun.org/products/agir-ici-et-maintenant-floreal-m-romero


Préface de Pinar Selek
Postface d’Isabelle Attard

Résumé
---------

L’effondrement qui vient n’est pas seulement celui des humains et de leur
milieu, mais bien celui du capitalisme par nature prédateur et sans limites.

Historiquement désencastré du social et nourri par l’exploitation et la
marchandisation des personnes, il étend désormais son emprise sur toute
la planète et sur tous les domaines du vivant.

C’est en se désengageant d’un constat fataliste et culpabilisant que nous
retrouverons une puissance d’agir ici et maintenant.

Quoi de mieux, pour cela, que de relire Murray Bookchin et d’appréhender
toutes les expérimentations et pratiques qui se développent après lui,
aujourd’hui, autour de nous ?

Floréal M. Romero dresse ici le portrait du fondateur de l’écologie
sociale et du municipalisme libertaire.
Il retrace son histoire, son cheminement critique et politique.

De l’Espagne au Rojava, en passant par le Chiapas, l’auteur propose, à
partir d’exemples concrets, des manières d’élaborer la convergence des
luttes et des alternatives pour faire germer un nouvel imaginaire
comme puissance anonyme et collective.

Essai autant que manifeste, ce livre est une analyse personnelle et
singulière de la pensée de Bookchin qui trouve une résonance bien au-delà
de l’expérience de l’auteur.
Il apporte des conseils pratiques pour sortir du capitalisme et ne pas
se résigner face à l’effondrement qui vient.
