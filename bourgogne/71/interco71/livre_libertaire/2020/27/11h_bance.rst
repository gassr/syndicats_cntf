

.. _rojava_bance_2020:

==========================================================================
11h Intervention de **Pierre Bance** sur l'expérience politique au Rojava
==========================================================================

.. seealso::

   - https://twitter.com/RojavaFrance
   - https://twitter.com/IntCommune
   - https://twitter.com/riseup4rojava2
   - http://www.kedistan.net/
   - https://twitter.com/KEDISTAN
   - https://twitter.com/goldman_bakou/status/1307698722361532419?s=20




**Pierre Bance**
==================

Pierre Bance, docteur d’État en droit, a été directeur des éditions Droit
et Société de 1985 à 2008.
Il collabore au site Autre futur.net, espace d’échanges pour un syndicalisme
de base, de lutte, autogestionnaire, anarcho-syndicaliste, syndicaliste
révolutionnaire (www.autrefutur.net).

Description
============

.. figure:: rojava_bance.jpg
   :align: center

   «La Fascinante Démocratie du Rojava – Le Contrat social de la Fédération
   de la Syrie du Nord» est le titre du nouveau livre de Bance à paraître
   aux éditions Noir et Rouge en octobre 2020.


Résumé du livre fait par les éditions Noir et Rouge
===============================================================

Pour mettre en place une société se réclamant des Droits de l’Homme et
de l’écologie sociale, fondée sur la commune autonome et le fédéralisme,
les acteurs civils et politiques de la Fédération démocratique de la Syrie
du Nord s’appuient sur un texte constituant, le Contrat social.

Ils tentent de concilier dans un même système politique, démocratie directe
et démocratie parlementaire.

Si les progrès en matière de droits et libertés sont considérables, le
fonctionnement démocratique des institutions fédérales est entravé par
le contexte géopolitique.

Les autorités comme la population sont-elles en capacité de dépasser
le stade d’une social-démocratie libertaire, pour parvenir à une société
sans Etat ou avec si peu d’Etat, le but annoncé ?


https://blogs.mediapart.fr/nestor-romero/blog/110620/la-fascinante-democratie-du-rojava
==========================================================================================


.. seealso::

   - https://blogs.mediapart.fr/nestor-romero/blog/110620/la-fascinante-democratie-du-rojava


Introduction
---------------

On parle beaucoup du Rojava, mais qu’en sait-on vraiment au-delà des
clichés guerriers ?

En 2017, les Éditions Noir et Rouge publiaient Un autre futur pour le
Kurdistan ? Municipalisme libertaire et confédéralisme démocratique écrit
par Pierre Bance.

En octobre sortira, de ce même auteur, **La Fascinante Démocratie du Rojava
Le Contrat social de la Fédération de la Syrie du Nord**.

Alors que le premier livre s’attachait à expliquer le municipalisme libertaire
et le confédéralisme démocratique, puis en recherchait la mise en application
en Turquie et au Kurdistan de Syrie, ce nouveau volume se penche sur
l’œuvre de la révolution du Rojava, et plus spécialement sur ses aspects
idéologiques, juridiques et et institutionnels.


Et demain ?
--------------


L’avenir de la Fédération démocratique de la Syrie du Nord est des plus
incertains. Demain, elle peut être envahie par les Turcs ou par Assad,
après une ultime trahison des Russes ou des Américains.
Si, par malheur, ce devait être le cas, pas plus que la Commune de Paris,
la Commune du Rojava ne mourra dans le cœur des femmes et des hommes
épris de liberté et d’égalité.

Son œuvre constructive restera et inspirera d’autres communes. Mais je
souhaite qu’elle vive pour mener à bien sa mission émancipatrice, pour
réaliser son ambition libertaire, nous prouver que la marche suivie
vers un autre futur était la bonne. Pour qu’elle ouvre de nouvelles
perspectives révolutionnaires.

Pierre Bance, La Fascinante Démocratie du Rojava. Le Contrat social de
la Fédération de la Syrie du Nord, Paris, Éditions Noir et Rouge, 2020, 500 pages, 25 euros.

Pré-commande par courrier aux Éditions Noir et Rouge - 75, avenue de
Flandre, 75019 Paris (frais de port gratuit pour une réservation avant parution).


Articles de David Graeber sur le Rojava
========================================

.. seealso::

   - https://theanarchistlibrary.org/search?query=david+graeber
   - https://theanarchistlibrary.org/search?query=david+graeber&sort=&filter_topic=%2Fcategory%2Ftopic%2Frojava

Tweets de David Graeber
---------------------------

.. seealso::


   - https://twitter.com/davidgraeber/status/1181891694326898688?s=20
   - https://twitter.com/davidgraeber/status/1181893321901400064?s=20


my theory is it's just the weird form racism against Middle Eastern people
takes: basically, most Euro-Americans consider them a bunch of savages,
violent Islamists by nature, & can't imagine anything better.

It's the leftists like the SDF who they see as dangerous & confusing 2/



David Graeber : Les Kurdes perdent un grand ami
--------------------------------------------------

.. seealso::

   - https://rojinfo.com/david-graeber-les-kurdes-perdent-un-grand-ami/
   - http://www.kedistan.net/2018/02/20/david-graeber-veritable-revolution/
   - https://davidgraeber.industries/
   - http://theanarchistlibrary.org/search?query=david+graeber
   - http://theanarchistlibrary.org/search?query=david+graeber&sort=&filter_topic=%2Fcategory%2Ftopic%2Frojava

Si il explique en 2015 au journal Le Monde séparer ses activités militantes
de son travail d’anthropologue, ses différentes publications s’inscrivent
dans la lignée de son engagement militant.

L’université de Yale refuse de le titulariser en 2005, pour des raisons
obscures probablement liées à son engagement politique. Après avoir
participé à l’émergence du mouvement Occupy Wall Street aux USA en 2011,
il avait affiché son soutien à la révolution en cours au Rojava.

On peut le voir dans cette interview de 2014 traduite sur le site Kedistan :

Il y déclare notamment :

“Et bien, si quiconque avait le moindre doute à savoir s’il s’agissait
vraiment d’une révolution, ou juste d’une espèce de façade, je dirais
que cette visite y a apporté une réponse définitive.
Il y a encore des gens qui parlent de cette façon : “ceci n’est rien
d’autre qu’une façade du PKK (le Parti des travailleurs du Kurdistan),
ils-elles ne sont qu’une organisation stalinienne autoritaire qui fait
semblant d’avoir adopté une forme radicale de démocratie.”

Non. Ils-elles sont tout à fait authentiques. Il s’agit d’une vraie révolution.”

A l’image de l’activiste kurde Dilar Dirik, nombreu.se.s sont les
militant.e.s kurdes qui lui rendent hommage.

Plus récemment, il s’était rendu à Paris pour observer le mouvement des
Gilets Jaunes.

Depuis son décès, les hommages se multiplient, montrant l’importance
des apports de Graeber à la réflexion politique dans les milieux de
la gauche radicale.
Il est aussi un des rares intellectuels dont l’action militante de terrain
irriguait les travaux universitaires. Avant sa mort, il travaillait à
un nouvel ouvrage sur l’histoire des sociétés non-hiérarchiques.

Graeber ne croyait pas à la verticalité du pouvoir, et s’est attaché
dans ses travaux à montrer que l’être humain vivait mieux dans une
société horizontale.

Interview • David Graeber : Non, il s’agit d’une véritable révolution !
---------------------------------------------------------------------------

.. seealso::

   - http://www.kedistan.net/2018/02/20/david-graeber-veritable-revolution/

On vous critique parfois en disant que vous êtes trop optimiste et trop
enthousiaste concernant ce qui se passe au Rojava ?
Est-ce vrai ? Ou est-ce que ceux qui vous critiquent ratent quelque chose ?

Je suis optimiste de tempérament, je recherche les situations porteuses
de promesses.

Je ne crois pas qu’il y ait de garantie que celle-ci va réussir au final,
qu’elle ne sera pas écrasée, mais elle ne réussira certainement pas si
tout le monde décide à l’avance qu’aucune révolution n’est possible et
refuse de lui apporter un soutien actif, ou même, consacre leurs efforts
à l’attaquer à accroître son isolement, ce que font plusieurs.

S’il y a une chose dont je suis conscient, et que d’autres ignorent,
c’est peut-être du fait que l’histoire n’est pas terminée.

Au cours des 30 ou 40 dernières années, les capitalistes ont déployé
d’immenses efforts afin de convaincre les gens que la situation économique
actuelle -pas même le capitalisme, mais cette forme particulière,
financiarisée et semi-féodale de capitalisme- constitue le seul système
économique possible.

Ils y ont consacré plus d’efforts qu’à véritablement créer un système
capitaliste mondial viable.

Conséquemment, le système s’effondre partout autour de nous, au moment
même où tout le monde a perdu la capacité d’imaginer autre chose.

Et bien, je crois qu’il est pas mal évident que dans 50 ans, le
capitalisme sous quelque forme que nous pourrions reconnaître, et
probablement sous quelque forme que ce soit, n’existera plus.

Quelque chose d’autre l’aura remplacé.

Ce quelque chose ne sera pas nécessairement meilleur.

Il sera peut-être pire.

Pour cette raison même, il me semble être de notre responsabilité, en
tant qu’intellectuels ou tout simplement en tant qu’êtres humains
réfléchis, de tenter au moins de penser à ce à quoi quelque chose de
meilleur pourrait ressembler.

Et s’il y a des gens qui sont en train de tenter de créer cette chose
meilleure maintenant, il est de notre responsabilité de les aider.


Hommage à David Graeber le 11 octobre 2020
--------------------------------------------


.. seealso::

   - https://twitter.com/goldman_bakou/status/1307268560499204096?s=20
   - https://davidgraeber.industries/memorial-carnival-fra
   - https://twitter.com/nikadubrovsky


.. figure:: hommage_David.png
   :align: center

   https://davidgraeber.industries/memorial-carnival-fra


Un carnaval commémoratif intergalactique pour David Graeber 11 Octobre 2020

Ceci est une invitation à se réunir à travers le monde pour un carnaval
commémoratif, dans l'esprit du seul et unique David Graeber, qui vient d
e nous quitter si soudainement et de manière si inattendue.

L'invitation émane de sa femme Nika et de quelques uns de ses ami.e.s.

David aurait été gêné par de longs éloges funèbres en costumes sombres,
par un cercle étroit d'ami.e.s proches. Comme il vivait plus pour la
révolution et le bouleversement du monde que pour la reconnaissance
personnelle, un enterrement triste axé uniquement sur le passé et sur
lui-même aurait mis David plus mal à l'aise que vivant.

Et maintenant, avec l'énorme trou en forme de David dans nos vies, il n'y a
jamais eu de meilleur moment pour vivre ses idées plutôt que pour
simplement s'en souvenir.

Pour David, l'anarchisme était "quelque chose que l'on fait" plutôt
qu'une identité, et c'est donc dans cet esprit espiègle et pragmatique
que nous avons décidé d'organiser un carnaval commémoratif pour lui,
un carnaval qui portera sur l'avenir : un avenir mystérieux et ludique,
débordant de solidarité. Un leitmotiv du carnaval est de rire face à la
mort, cela peut être la chose la plus utile à faire dans des situations
horribles.

Comme nous le savons tous, David adorait plaisanter. D’ailleurs, ses
derniers mots étaient une blague.

David était comme un chat ; il a eu de nombreuses vies. Lorsque les gens
le rencontraient, via twitter ou même en lisant ses livres ou en assistant
à ses conférences, beaucoup devenaient instantanément ses ami.e.s proches,
sa famille élargie et ses interlocuteur.trices.
Parmi les ami.e.s de David, il y avait tant de personnes qui ne se seraient
jamais rencontrées.

La mosaïque d'habitant.e.s de Portobello Road, des blogueurs solitaires,
des professeurs d'université, des migrant.e.s sans papiers, plusieurs
générations de militant.e.s, d'artistes, de musicien.ne.s de rock et
de nombreux jeunes – étudiant.e.s, rebelles, membres de mouvements sociaux.

Tou.te.s ont senti que David faisait partie de leur vie et beaucoup veulent
continuer son travail et rester proches de lui.
C'est comme s'il avait 50 000 frères et sœurs et 200 000 meilleur.e.s
ami.e.s, et c'est pourquoi le carnaval commémoratif pour David ouvrira
partout un espace pour tou.te.s celles et ceux d'entre nous qui veulent
continuer à se sentir proches de lui.

Il est mort à Venise, une ville qu'il visitait souvent.

**David aimait se déguiser dès qu’il en avait l’occasion**.
Il ramenait des masques et des costumes vénitiens après chaque visite.
Avant de devenir un produit touristique, le carnaval de Venise constituait
un espace politique de démocratie radicale.

Pendant le carnaval, il n'y avait ni noir.e.s, ni blanc.he.s, ni vieilles,
ni vieux, ni jeunes, ni belles, ni beaux, ni laid.e.s, ni pauvres, ni
riches. Tout le monde était un masque.

Acteur des mouvements anticapitalistes des années 90 et des années 2000,
il connaissait les irrésistibles similitudes entre l'expérience d'un
carnaval et celle d'une insurrection.

Il y a exactement 9 ans aujourd'hui, le 17 septembre 2020, une invitation
a été entendue et un mouvement est né.
L'invitation disait simplement : "Occupez Wall Street - Apportez une tente".

David était l'une des dizaines de milliers de personnes qui ont répondu
en s'organisant et en occupant.
Le reste appartient à l'histoire.

Aujourd'hui, nous vous invitons à organiser un carnaval commémoratif
pour David, où que vous soyez, le dimanche 11 octobre 2020.

Inspirés par le principe du micro ouvert du mouvement Occupy, nous vous
demandons, à un moment donné de votre carnaval, d'ouvrir un espace pour
que les gens puissent parler et partager des idées. Ces assemblées peuvent
s'inspirer de la vie et des paroles de David et de la façon dont nous
pouvons les incarner dans un avenir qui commence maintenant - "vivez
comme si vous étiez déjà libre", aurait dit David.

Que vous soyez seul.e chez vous et que vous vouliez juste lire votre
passage préféré de son œuvre, ou un collectif d'activistes désirant
envahir les rues avec une assemblée de masse ; que vous soyez un
groupe d'universitaires dans une salle de séminaire ou des combattant.e.s
en première ligne ; que vous soyez dans un squat ou en excursion
anthropologique, dans un camp de protestation ou un musée, n'importe
quel endroit peut accueillir un carnaval commémoratif.

La règle est simple : "Apportez un masque" (plus dans le style carnaval que covid, bien sûr).

Des dizaines d'événements sont déjà prévus, notamment au Zuccotti Park NY,
**au Rojova**, sur la zad, en Corée, en Autriche, à Berlin, à Londres.

Si vous envisagez d'organiser un événement, envoyez-nous un courriel nous
indiquant quoi et où, afin que nous puissions le mettre en ligne pour


que les gens puissent y participer. Nous organiserons un streaming en
ligne des carnavals commémoratifs, dont les détails seront communiqués
ultérieurement, ainsi que des moyens de coordonner autant de fuseaux horaires.

email: carnival4david@riseup.net

Bien à vous, dans le deuil… et s’en organisant

Nika (https://twitter.com/nikadubrovsky) et ses ami.e.s
