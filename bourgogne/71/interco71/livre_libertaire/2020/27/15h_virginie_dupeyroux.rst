
.. _virigine_dupeyroux_2020:

==========================================================================================================
15h **Amiante et mensonge : notre perpétuité - Journal de Paul et Virginie"** par **Virginie Dupeyroux**
==========================================================================================================

.. seealso::

   - https://www.editions-verone.com/auteur/virginie-dupeyroux/amiante-et-mensonge-notre-perpetuite-journal-de-paul-et-virginie/
   - http://amiante-et-mensonge-notre-perpetuite.com/presse-interviews-audio-virginie-dupeyroux
   - https://www.over-blog.com/user/2716380.html
   - https://www.monde-libertaire.fr/?article=Amiante_et_mensonge_:_notre_perpetuite_journal_de_Paul_et_Virginie
   - https://trousnoirs-radio-libertaire.org/sons/463_17aout2020.mp3
   - http://www.librairie-publico.info/?p=4544
   - https://twitter.com/goldman_bakou/status/1307693580794945537?s=20




**Paul Dupeyroux**
=====================


.. figure:: amiante_et_mensonges.png
   :align: center

   http://amiante-et-mensonge-notre-perpetuite.com/presse-interviews-audio-virginie-dupeyroux

.. figure:: paul-dupeyroux-chien-stanley.jpg
   :align: center

   https://www.divertir.eu/blog/culturel/amiante-le-temoignage-de-paul-et-virginie-dupeyroux.html


**Virginie Dupeyroux**
===========================

.. seealso::

   - https://www.over-blog.com/user/2716380.html
   - http://amiante-et-mensonge-notre-perpetuite.com/presse-interviews-audio-virginie-dupeyroux

.. figure:: virginie_dupeyroux_2.jpg
   :align: center

Comme des milliers d'autres personnes, je suis la fille d'une victime de
l'amiante : Paul Dupeyroux, mon père, est l'une des nombreuses victimes
de l'ancienne usine de broyage d'amiante CMMP (Comptoir des Minéraux et
Matières Premières) d'Aulnay-sous-Bois (93600).

Membre de Ban Asbestos France, de l'association Henri Pézerat et de
l'Adeva Centre, je suis l'auteure du livre "Amiante et mensonge : notre
perpétuité - Journal de Paul et Virginie" (Éditions Vérone).


Description édition verone
===========================

.. seealso::

   - https://www.editions-verone.com/auteur/virginie-dupeyroux/amiante-et-mensonge-notre-perpetuite-journal-de-paul-et-virginie/

Tu es né à Aulnay-sous-Bois, à l'été 1942. Au mauvais endroit, au mauvais
moment... un moment qui dura plus de cinq décennies...

Tu as grandi sans le savoir, comme tous tes amis d'enfance, près d'une
usine de broyage d'amiante installée en pleine zone pavillonnaire.

Tu as quitté le Vieux Pays en 1961. Le cancer de l'amiante t'a rattrapé
en juin 2014. Paul, mon Père, mon meilleur ami. Travailleur, fraternel,
libertaire. Juste.

Nous sommes tombés sur des gens de médecine malhonnêtes, dignes héritiers
des membres du Comité Permanent Amiante (1982-1995).
Nous avons cru en leur probité. Nous avons lutté, ensemble.

Ce livre retrace notre parcours, calendrier autobiographique sur
quinze mois dans le monde médical à Nevers, dans la Nièvre.

Nous y exprimons à deux voix la stupéfaction de la découverte de ta
maladie, puis la compréhension de l'origine de ton empoisonnement.
Nous y dénonçons l'irrespect médical et la maltraitance dont sont
trop souvent victimes les patients atteints de pathologies uniquement
imputables à l'amiante, et le mensonge médical qui complète le mensonge
industriel et le couvre. Nous subissons.
Nous sommes des millions. Nous faisons des allers-retours au cimetière.
Nous enterrons nos morts.




Sur trousnoirs radio-libertaire 89.4Mhz le lundi 17 août 2020
===============================================================

.. seealso::

   - https://trousnoirs-radio-libertaire.org/sons/463_17aout2020.mp3

Le scandale de l’amiante n’est pas derrière nous : plus de 100 000 décès
constatés depuis 1995, dix personnes tuées par jour en moyenne aujourd’hui,
plus de 100 000 morts « attendus » d’ici 2050...

Ses fibres, présentes dans bâtiments, hôpitaux, écoles, élevages s’accumulent
dans le corps et sont à l’origine de cancers foudroyants, surgissant des
dizaines d’années plus tard.

Nous recevons Virginie dupeyroux pour son livre
**amiante et mensonge : notre perpétuité – Journal de Paul et Virginie**

Paul, mon père, est l’une des nombreuses victimes de l’ancienne usine
de broyage d’amiante CMMP d’Aulnay-sous-Bois. Ce fléau rendu volontairement
invisible, est livré par les industriels de l’amiante, et plus largement
par ceux des pesticides, du nucléaire pour le profit, au prix de la vie
des ouvriers et des riverains de leurs usines ! »

« Je ne cesserai de d’informer et de dénoncer le crime social de l’amiante ».

Pour cela, elle fait partie de l’association Ban Asbestos France, de
l’association Henri Pézerat et de l’Adeva (Association des victimes de
l’amiante) région centre.


Le 25 décembre 2019 dans le monde libertaire
===============================================

.. seealso::

   - https://www.monde-libertaire.fr/?article=Amiante_et_mensonge_:_notre_perpetuite_journal_de_Paul_et_Virginie


J’ai lu avec beaucoup d’intérêt ce livre Amiante et mensonge: notre
perpétuité, journal de Paul et Virginie qui s’est révélé pour moi, être
une somme d’émotions : la crainte, l’espoir quand les bilans de santé
de Paul s’amélioraient, les cruelles déceptions lors de reflux, la
colère.. toutes émotions notifiées jour après jour par Paul, que dépeint
Virginie,sa fille, comme un être sensible, cultivé, ouvert aux autres
tout en n’oubliant pas une bonne part de vigilance envers certaines
catégories humaines, ce qui s’avérera juste, par la suite.

Paul était en outre comme Virginie, un grand ami des idées libertaires,
un antimilitariste radical, un défenseur permanent de l’environnement
et faisait partie du comité de soutien à Léonard Pelletier.

Cet agenda, Virginie Dupeyroux le tient, simultanément avec son courage,
sa pugnacité, son soutien continu envers son père, son entourage, sa
compagne particulièrement, dans cette lutte implacable contre la maladie.

Il faut se rappeler que l’amiante fut déclarée nocive dès l’aube du
XX° siècle ( 1906 ) et ne fut interdite dans son utilisation, en France,
seulement qu’en fin de ce même siècle, c’est-à-dire en 1997 ; entre temps,
d’innombrables souffrances, décès, et la série n’est pas close [note] .

Donc, cette colère, Virginie la dirige d’abord contre l’entreprise CMMP [note] ,
broyeuse d’amiante, et surtout de vies, car Paul, dès sa prime enfance,
longeait ses murs pour aller à l’école [note] , ainsi que ses petits
camarades, qui furent rattrapés également par la maladie, pour la plupart.

Ce fléau s’introduisit dans le corps de Paul, dès son plus jeune âge, et
ne se déclara brutalement que des décennies plus tard, quand il
atteignit 70 ans environ [note] .

Cette entreprise, et ses semblables, ne furent jamais réellement inquiétées,
défendues par la loi, et toute une batterie d’avocats, tous plus ou moins
corrompus, et vendus à la cause patronale [note] .

La colère n’épargne pas non plus les services médicaux qui ne tardèrent
pas à manifester leur inhumanité, leur désinvolture, car n’hésitant pas
à balader Paul d’un service à l’autre, leur incompétence à proposer un
remède inefficace, sauf une petite lueur d’espoir qui s’alluma dans
une entité médicale, fort éloignée géographiquement du domicile de
Paul ( fixé près de Nevers, comme Virginie ), mais il était trop tard.

Enfin, cette juste colère n’oublie pas les soi-disant responsables
politiques, de gauche comme de droite, de Claude Evin [note] à Marisol Touraine
par exemple, qui ne levèrent pas le petit doigt, n’appuyèrent en aucune
façon Virginie dans ses démarches, recours déclenchés par elle et les
associations comme ANDEVA [note], qu’elle avait rejointes, pour que la
vérité enfin éclate.

Guy (groupe de Rouen )

Amiante et mensonge : notre perpétuité, journal de Paul et Virginie, éditions Vérone, 25 €
N.B. : les bénéfices des ventes sont reversés à Ban Asbestos France, Association Henri Pézerat


Dimanche 8 décembre 2019 par le groupe Salvador Segui
========================================================

.. seealso::

   - http://www.librairie-publico.info/?p=4544
   - https://paris-luttes.info/amiante-silence-et-mensonge-12936

.. figure:: virginie_dupeyroux_fa_2019_12.jpg
   :align: center



Le groupe Salvador-Seguí de la Fédération anarchiste organise une
rencontre/débat autour du livre Amiante, silence et mensonge en
présence de l’autrice, Virginie Dupeyroux à la librairie du
Monde libertaire (Publico).

Amiante : silence et mensonge, autour des souffrances et préjudices subis
par les victimes de l’amiante.

Silence et mensonge médical complétant et couvrant le mensonge industriel,
et tendant à faire passer les cancers dont sont atteintes les victimes
de l’amiante pour une fatalité.

Actuellement encore, chaque jour, 10 décès en France et 102 dans le
monde sont imputables à l’amiante.

Les industriels en pleine connaissance de cause ont tenté de maintenir
son usage. Non seulement dans les lieux de travail, mais aussi dans
l’habitat et tout notre environnement.

Depuis 25 ans, ce scandale est dénoncé par des particuliers et des
associations (comme Ban Abestos France) se fixant comme objectif
l’interdiction mondiale de l’amiante.

Ce combat a permis de briser l’invisibilité des victimes de maladie liée
à l’amiante.

**Combat toujours en cours, inutile de préciser.**

Le livre de Virginie Dupeyroux est un cri d’alarme et de colère qui
participe pleinement à ce combat contre le silence et le mensonge.


https://www.divertir.eu Amiante : le témoignage de Paul et Virginie Dupeyroux le dimanche, 25 mars 2018
==========================================================================================================

.. seealso::

   - https://www.divertir.eu/blog/culturel/amiante-le-temoignage-de-paul-et-virginie-dupeyroux.html

Qu'est ce qui vous a poussé à écrire un livre sur votre père Paul, disparu
à cause d'un cancer du à l'amiante ?

Je tenais avant tout à ce que mon père ait la parole. Il prenait des notes
sur tous les événements de sa vie, tous les endroits où il a travaillé.
Il l’a fait durant sa maladie.
Le témoignage direct d’une victime de l’amiante, qui plus est atteinte
de mésothéliome, un cancer uniquement imputable à cette fibre, m’a semblé
capital, afin que les responsables de ce scandale sanitaire et environnemental
soient mis devant les conséquences de leurs actes délibérés, devant la
souffrance physique et morale relatée au quotidien.

Par ailleurs Paul, que j’appelais par son prénom, m’avait demandé d’« aider
à faire cesser cet état de non-droit ». L’écriture m’a paru la première
étape pour accéder à sa demande. Paul étant une victime environnementale,
le but est aussi de faire savoir que tout un chacun peut malheureusement
être concerné. Les effets différés d’une exposition à l’amiante sont dévastateurs.

Deux autres points m’ont semblé essentiels : relater le mensonge et
l’« indifférence coupable », comme le dit si justement Annie Thébaud-Mony
dans la préface, d’un certain corps médical ; les victimes de l’amiante
subissent bien trop souvent la double peine. Ensuite, faire connaître
davantage les ravages du CMMP, qui a broyé de l’amiante bleu, le plus
toxique, en plein cœur d’une zone pavillonnaire à Aulnay-sous-Bois,
pendant plusieurs décennies. C’était une petite structure, et son nom
est moins connu qu’une multinationale de l’amiante comme Eternit, mais
cette entreprise ne doit pas être oubliée dans la liste des empoisonneurs.

Enfin, ce livre est aussi un hommage à mon père, mon meilleur ami.
Un homme courageux, souvent parti travailler à l’autre bout du monde,
pour que ma mère et moi ne manquions de rien. Très courageux, aussi,
dans l’épreuve.

Votre père et une partie de votre famille ont été contaminés car ils
résidaient à proximité d'une usine de retraitement de l'amiante
(le CMMP à Aulnay sous Bois). Selon vous pourquoi l'affaire a du mal
à être connue publiquement ?

Les pouvoirs publics ont tout fait pour cacher ce scandale. Dès les
années 30, des riverains s’organisaient et diffusaient des pétitions.
Les autorités n’en ont jamais tenu compte. Les poussières polluaient
tout le quartier. Le CMMP ne respectait pas la réglementation des normes
de sécurité. Malgré tout, il a continué à sévir des décennies durant.
Officiellement, l’usine poison broyait des matières telles que la silice
et l’oxyde de fer. La préfecture de Seine-Saint-Denis a tout fait pour
nier le broyage et la commercialisation d’amiante dans cette usine.
Forcément, ce sont les pouvoirs publics qui avaient donné l’autorisation
d’exploitation. De plus, cette entreprise travaillait sous tutelle de
l’État. Qui a donc légalisé la mort programmée des ouvriers, riverains
et écoliers alentour.
Lorsque Monsieur et Madame Voide, lanceurs d’alerte de cette affaire,
ont voulu avoir accès aux documents officiels concernant le CMMP, dès
1996, année où Pierre Léonard, frère de Nicole Voide, est décédé d’un
mésothéliome à l’âge de 49 ans par le simple fait d’avoir été riverain
du CMMP, l’accès aux documents officiels leur a été refusé cinq années
durant ! Mais la ténacité paie. C’est aujourd’hui grâce à eux que la
vérité s’est faite.

Pouvez-vous nous résumer comment votre père a été diagnostiqué et
informé sur sa maladie ? En combien de temps le cancer l'a-il emmené ?
Mon père, en juin 2014, se plaignait d’un mal qui lui faisait penser à
un point de côté présent en permanence. Les examens passés ont révélé
une pleurésie (formation d’un liquide) dans le poumon droit.
Des ponctions ont été effectuées. Les analyses ne révélaient rien
d’alarmant. Mais le tep-scan, lui, a fini par démontrer la présence
d’un cancer dans le lobe inférieur droit de sa plèvre pariétale
(la plèvre enveloppe les poumons ; elle est composée de deux feuillets,
viscéral et pariétal ; la plèvre pariétale est la plèvre extérieure).
On nous parle alors seulement de la présence de « quelques cellules
cancéreuses » et on nous affirme que, dans tous les cas « des traitements
existent ». Des médecins neversois affirment à mon père qu’un « talcage »
est nécessaire, intervention qui, en insufflant quelques grammes de talc,
permet de coller les deux parois de la plèvre afin qu’elles n’émettent
plus de liquide. L’intervention a lieu le 7 août 2014. Ce n’est que le
8 septembre que, malgré nos demandes incessantes pour avoir accès aux
résultats des prélèvements, on nous informera de la présence d’un
mésothéliome. C’est ce jour-là que le grand mensonge médical débutera,
sur l’efficacité du protocole, national, appliqué également dans de
nombreux pays dans le monde. Un an et une semaine plus tard, mon père
quittait ce monde dans des souffrances terribles, sans avoir eu la
chance d’accéder à un essai thérapeutique ; je l’explique longuement
dans notre livre.

Paul est né à Aulnay sous Bois, en dehors du contexte de l'amiante il
en avait gardé quelques bons souvenirs. Est ce qu'il y en a un qui vous
a le plus marqué ?
Paul n’avait gardé que de bons souvenirs d’Aulnay : une enfance magnifique,
avec sa bande de copains, notamment Ahmed et Daniel, le foot auquel il
jouait à Villeparisis. Il avait une grande volière et prenait soin de
ses oiseaux. Il adorait le Parc des Cygnes où il se rendait après l’école
(l’école du Bourg, l’un des trois établissements de l’enseignement primaire
contaminé par le CMMP). Ce qui le faisait rire dès qu’on l’évoquait,
c’était les bons moments passés en classe avec son copain Jean-Pierre
qui, au lieu d’écouter leur instituteur, avait créé une bande  dessinée
avec un personnage totalement délirant qui, me disait Paul, ressemblait
au Pangolin décrit à la manière de Pierre Desproges.

Il y a plusieurs années vos parents avaient décidé de partir à la campagne
pour profiter de meilleures conditions de vie. Ils avaient notamment
plusieurs animaux qui vous ont à la fois soutenu psychologiquement ;
mais certains pour qui vous avez de la tristesse puisque qu'une partie
d'entre eux sont décédés dans des circonstances particulières.
Est ce que vous pouvez nous en parler ?
Ma famille paternelle avait quitté Aulnay dès 1961. Je suis venue au monde
dans la Nièvre, où mes parents se sont rencontrés. Paul aimait infiniment
les animaux, et notre passion était d’en récupérer et de les ramener à
une vie aimante, à laquelle tout le monde est en droit de prétendre.
Durant la maladie de mon père, notre poney Dalton a été empoisonné.
Nous avions récupéré Dalton 19 ans auparavant pour qu’il ne finisse pas
à la boucherie. Sa perte a été d’autant plus dure à supporter au vu de
ce que nous vivions. Dans mon livre, je mentionne aussi Stanley, notre
épagneul breton. Paul et Stanley étaient inséparables. Ils s’adoraient.
Quand mon père a quitté ce monde, Stanley était d’une tristesse infinie.
Il était à mes côtés pendant toute la rédaction de ce livre, la postface
excepté ; elle lui est consacrée : Stanley a lui aussi développé un
mésothéliome. Comme pour mon père, il n’y a eu aucun signe avant-coureur.
Des fibres d’amiante se sont logées dans son péricarde, l’enveloppe qui
entoure le cœur, puis les cellules cancéreuses ont migré à la plèvre.
Nous avons tout tenté pour le sauver. Il a été opéré à l’hôpital
vétérinaire Frégis, à Arcueil. Stanley ne chassait pas, était un
épagneul « de canapé ». Il paraissait si jeune. Excepté les quatre
premiers mois de sa vie, il n’avait vécu que chez nous. Il n’y a pas
d’amiante dans notre maison, ni dans notre environnement. Nous avons
retrouvé la source de sa contamination en regardant les photos prises
sur le lieu de son adoption, un élevage, en février 2006 à Gilly-sur- Loire (71).

La toiture en amiante-ciment était dégradée. Quant aux murs, que
contenaient-ils ? J’ai réclamé une enquête à la Direction Départementale
de Protection des Populations. L’amiante-ciment (appelée couramment
« fibrociment ») est tout aussi nocif. Les villes et les campagnes en
sont truffées…
Le décès de Stanley, que nous avons dû aider à partir, a été et reste
un traumatisme.

Je suis antispéciste, c’est-à- dire que je ne fais pas de différence
entre les espèces. Nous n’avons pas d’enfant, c’est un choix. Stanley,
comme notre petite épagneule adoptée en Bretagne, était aimé comme un enfant…
Paul Dupeyroux et son chien Stanley

Les chanteurs et écrivains sont très présents dans cette période de
votre vie. S'il y avait un livre et une chanson qui devaient ressortir
de ce moment quels seraient-ils ?
Je vais commencer par la chanson. Si je devais choisir, je prendrais
Si nous mourons, sous-titrée « Lettre d’Etel Rosenberg à ses enfants ».
C’est une chanson de Jean Ferrat. Mon père adorait ce texte. Etel et
Julius Rosenberg étaient un couple de communistes américains, arrêtés,
jugés coupables d’espionnage au profit de l’URSS et exécutés en 1953.
Ils étaient innocents. Tout comme le sont les victimes de l’amiante.
Les victimes professionnelles qui ont « perdu leur vie à la gagner »
puisqu’on leur a caché des décennies durant la cancérogénicité de cette
fibre, au nom du profit ; les victimes environnementales qui ont eu la
malchance de vivre au mauvais endroit, ou d’être en contact avec, par
exemple, des vêtements de travail contaminés par l’amiante.
Pour le livre, ce serait Enterre mon Cœur à Wounded Knee de Dee Brown,
sur le massacre des Sioux Lakota à Woundee Knee par l’armée américaine,
en décembre 1890. Je pense que ce serait aussi le choix de Paul, pour
que ce massacre ne soit jamais oublié. Il y a là aussi un parallèle criant.
Si j’avais dû choisir un film, j’aurais pris Voyage au bout de l’enfer
de Michael Cimino. Ce titre résume bien notre histoire, comme celle de
tant d’autres victimes et de leurs familles.

Comment avez-vous choisi la photo de Paul, votre père, en couverture ?

Ce choix m’a paru évident car la photo a été prise à l’École du Bourg,
à Aulnay, en 1948. L’établissement a subi de plein fouet la contamination
du CMMP. Paul portait déjà en son corps des fibres d’amiante, sans le savoir.
Cette photo montre un gosse innocent, qui ne demandait rien à personne,
un petit garçon heureux, qui lance aussi un regard accusateur aux
industriels qui l’ont empoisonné : « Regardez ce que vous avez fait ! ».

Sur le site de Ban Asbestos France, dans l’historique de la lutte citoyenne
contre le CMMP, Nicole Voide a aussi choisi une photo de son frère Pierre,
prise à l’école du Bourg.

Dans Amiante et mensonge : notre perpétuité, vous mettez des photos de
votre famille quand vous apprenez enfin le retraitement de l'amiante
du CMMP par voie de presse. Pourquoi ce choix ?
Parce que c’est un moment crucial : celui où nous comprenons que c’est
le CMMP qui est à l’origine de la contamination à l’amiante de ma
grand-mère, Simone, emportée en dix mois par un mésothéliome, en juin 1978.

Jusqu’ici, nous ne comprenions pas où elle avait pu respirer cette fibre.
Il n’y avait pas d’amiante dans son environnement : elle vivait depuis
dix-sept ans à la campagne, avait une vie saine. À l’époque de sa maladie,
aucun personnel médical n’évoque l’amiante. Nous avons compris l’origine
de son cancer quand les médias ont parlé des premières plaintes de
victimes au milieu des années 90. Mais nous nous interrogions encore
sur le lieu de sa contamination. Le temps avait passé mais pour mon père,
le cadet de ses enfants, ce n’était pas une affaire classée.
La lumière ayant été faite, car personne dans la famille ne savait
quelle avait été l’activité de cette usine (et c’était le cas de très
nombreux riverains), c’est ce moment que j’ai choisi pour présenter
visuellement les victimes de ma famille.

En dehors de quelques sujets du Parisien et de France Télévisions que
vous évoquez dans le livre, l'amiante est rarement couvert par les médias.
Comment expliquez-vous cette situation et qu'est-ce que ça vous évoque ?
L’État et ses dirigeants, quels qu’ils aient été ou quels qu’ils soient,
ont tout fait pour que le scandale de l’amiante soit le moins possible
médiatisé, pour que les victimes de l’amiante deviennent les invisibles
de notre société. Et la stratégie a été la même dans de nombreux endroits
du globe ! Trop de personnalités se sont enrichies avec cette fibre
assassine, alors que ses effets mortifères étaient démontrés depuis… 1906 !
Des politiques de tous bords ont été impliqués, des scientifiques, des
médecins ont accepté de mentir au profit des industriels en niant la
toxicité de l’amiante, puis en en prônant l’usage contrôlé.
Annie Thébaud-Mony l’explique bien mieux que moi dans La science asservie,
aux Éditions La Découverte (2014). Une stratégie du doute a été mise en
place par ces industriels, et par ceux qu’ils avaient soudoyés.
Elle a conduit à retarder l’interdiction de l’amiante, seulement en 1997,
pour ce qui concerne la France. Le Comité Permanent Amiante (1982-1995),
lobby des industriels, a particulièrement œuvré à cette tragédie.
Certains de ses membres sont toujours en vie, et la stratégie actuelle
vise à tout faire pour ne pas mettre en cause ces complices, qui ont
(devraient avoir ?) sur la conscience des milliers de morts.

Vous évoquez également le dédain de l'hôpital lors du traitement de
votre père dans votre livre ; ainsi qu'on vous ait caché des essais
thérapeutiques qui seraient plus efficaces et pratiqués dans 3 hôpitaux
français sous prétexte d'un "protocole national unique". La France
perd-elle la hauteur et la réputation qu'elle avait dans le traitement
de ses malades et qu'est-ce que cette situation vous inspire avec le recul ?

Les gens de médecine qui avaient le sort de mon père entre leurs mains
déshonorent clairement, pour moi, et la profession médicale, et le serment
d’Hippocrate. Ils pratiquent une médecine de classe.
De nombreuses victimes de l’amiante en ont subi les conséquences.
Il est certain que ces personnes ne font pas honneur à qui que ce soit.
Nous avons tous des devoirs et des droits. Je pense pouvoir dire que ma
famille, comme tant d’autres, s’est toujours acquittée de ses devoirs,
qu’ils soient électoraux, citoyens… Mais nos droits ont été méprisés.
Nous avons été traités comme des citoyens de seconde zone. Personne ne
devrait jamais avoir à subir cela.


Amiante et mensonge : notre perpétuité - Journal de Paul et Virginie"
========================================================================

.. seealso::

   - http://amiante-et-mensonge-notre-perpetuite.com/pourquoi-ce-livre

Mon livre, "Amiante et mensonge : notre perpétuité - Journal de Paul et Virginie",
est la concrétisation d'une promesse faite à mon Père, Paul.

Un homme fraternel, travailleur, libertaire et libre penseur, juste.

Paul était et demeure mon meilleur ami. Il avait pris des notes tout au
long de sa maladie. il prenait des notes sur tout : sur son travail,
sur ses voyages, sur les ouvrages qu'il lisait, sur le triste état de
ce monde...
Il pensait se sortir de cette terrible maladie, puisque le mésothéliome
ou "cancer de l'amiante" dont il était atteint était très localisé, sur
sa plèvre pariétale.

Paul a reçu un traitement, protocole mondial pour le mésothéliome, sur
lequel les médecins auxquels nous avons eu affaire nous ont menti.

Sur la rémission, également, puis sur le second protocole.

Lorsqu’il a quitté ce monde, j’ai décidé de publier ses écrits, en
parallèle aux miens.

"Aide à faire cesser cet état de non-droit", m'avait demandé Paul.
