
===================================================================================================================================
16h30 Espoirs déçus Engagements antifranquistes et libertaires durant la «transition démocratique» espagnole par **David Rappe**
===================================================================================================================================

.. seealso::

   - http://www.atelierdecreationlibertaire.com/Espoirs-decus.html
   - https://twitter.com/goldman_bakou/status/1307691405486284803?s=20


.. figure:: espoirs_decus.png
   :align: center



**David Rappe**
=================

David Rappe est enseignant d’Histoire-Géographie à Vaulx-en-Velin dans
la région lyonnaise. Il a réalisé plusieurs travaux sur les Bourses du
travail de la région Rhône-Alpes.

Il milite à la Fédération des travailleurs-ses de l’éducation de la CNT
et depuis de nombreuses années au sein de l’Union locale de Lyon de
la Fédération anarchiste.

David Rappe, historien, militant syndicaliste et libertaire, a déjà publié
la Bourse de travail de Lyon - Une structure ouvrière entre services
sociaux et révolution sociale, 2004, et Rendez-vous avec Armand Gatti -
Dix rencontres avec Armand Gatti, 2008.

Description
============

Structurée autour de la trajectoire militante «espagnole» de ­
**Bernard ­Pensiot** (1948-2018) – qui lui valut, comme son copain Victor Simal,
d’être encabané à la Modelo de Barcelone huit mois durant –, l’étude de
David Rappe relève d’un double pari : rendre hommage à cet activiste
de l’ombre et tenter, sans mythification ni simplification historique,
de restituer ce qui se joua autour de cette météorique reconstruction-déconstruction
de la CNT (Confédération nationale du travail) de la fin des années
soixante-dix.
Période qui amena aussi Bernard à se consacrer, lors de sa détention, au
grand mouvement des prisonniers d’Espagne regroupés dans la Coordination
des prisonniers en lutte (COPEL) pour l’amnistie générale.

Espoirs déçus marque un nouveau jalon dans l’histoire de cette période
où, par un effet un peu mécanique de volontarisme et de réémergence
mémorielle conjugués, la CNT sembla retrouver une clarté seconde et
réinventer un possible perpétuel.
À vrai dire, nous y avons cru, ou plutôt nous croyions qu’elle avait
toutes les raisons de renaître, porté·es que nous étions par l’illusion
qu’aucun mouvement libertaire conséquent ne pouvait exister en Espagne,
terre d’anarchisme par excellence, privé de son axe central, à savoir
son organisation de classe…

La focale qu’adopte **David Rappe** dans son texte offre une vision éclairante
sur une frange remuante du jeune mouvement libertaire espagnol qui, à ­
partir de 1976, se développa, sur les marges d’une CNT ouverte à tous
les vents, en recyclant des pratiques quotidiennistes issues de 1968,
corrélées à certaines ­appétences pour le spectacle de la lutte armée.
C’est une histoire d’autant plus mal connue que celles et ceux qui la
firent, la vécurent et, pour nombre de ses protagonistes, s’y brûlèrent
les ailes  ; ils se voulaient plus adeptes des parcours buissonniers
que des sentiers balisés.

C’était ne rien comprendre à la logique même de ladite transition-transaction
«démo­cratique» dont le principal projet, de «droite» postfranquiste
et de «gauche» antifranquiste, consistait précisément à marginaliser
le franquisme systémique et oligarchique avant de le démanteler, condition
nécessaire pour rallier le monde «libre» de la démocratie représentative
du profit maximal.

Autrement dit, on changeait d’époque. L’Espagne avait cessé de se
prétendre «différente» : elle voulait être désormais une partie du
Tout-Capital, la pointe avancée du Grand ­Marché européen.
