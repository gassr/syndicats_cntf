
=================================================
Lecture de **David Rougerie** en fin de matinée
=================================================


**David Rougerie**
===================

.. seealso::

   - http://www.davidrougerie.sitew.com/


David Rougerie travaille aux P.T.T. (Poètes Tout Terrain)
car sa première langue est : Poésie non dénuée d'Humour.
Artiste indépendant, itinérant et citoyen,
auteur, interprète et metteur en scène
au théâtre, au cabaret, à la chanson, + si affinités...
(cinéma, radio, performance, rue, lecture...).
Priorité : les mots.
Mais aussi, présence silencieuse...
Egalement  écrivain (10 livres fin 2020)
