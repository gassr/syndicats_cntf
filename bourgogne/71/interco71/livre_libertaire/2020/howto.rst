
.. _howto_cluny:

===========================
Informations géographiques
===========================

.. seealso::

   - https://fr.wikipedia.org/wiki/Cluny_%28Sa%C3%B4ne-et-Loire%29




Pour se rendre à Cluny
============================


Itinéraire Grenoble => Cluny
----------------------------------

.. seealso::

   - https://www.openstreetmap.org/directions?engine=fossgis_osrm_car&route=45.1876%2C5.7358%3B46.4339%2C4.6576#map=8/45.257/6.422

Distance: 202km. Temps: 2:16


.. figure:: ../../grenoble_cluny.png
   :align: center

   https://www.openstreetmap.org/directions?engine=fossgis_osrm_car&route=45.1876%2C5.7358%3B46.4339%2C4.6576#map=8/45.257/6.422


Carte de cluny
================

.. seealso::

   - https://www.openstreetmap.org/#map=15/46.4303/4.6712

.. figure:: ../../carte_cluny.png
   :align: center

   https://www.openstreetmap.org/#map=15/46.4303/4.6712


Salle des Griottons, parc Abbatial 71250 Cluny
================================================

.. seealso::

   - https://www.gralon.net/evenements/71/programme-salle-des-griottons-22345.htm


.. figure:: cluny.png
   :align: center


.. figure:: ../../salle_des_griottons.png
   :align: center

   https://www.gralon.net/evenements/71/programme-salle-des-griottons-22345.htm
