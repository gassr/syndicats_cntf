
.. index::
   pair: syndicats; CNT Poitou-Charentes
   pair: Union Régionale ; Poitou-Charentes
   ! Poitou-Charentes


.. _union_regionale_poitou_charentes:

=======================================================
Union Régionale Poitou-Charentes
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Poitou-Charentes


.. figure:: poitou_charentes.png

   *La région Poitou-Charentes*


:code région: 54


Le Poitou-Charentes est l'une des 22 régions de la France métropolitaine.
Elle regroupe quatre départements :

- Charente (16)
- Charente-Maritime (17)
- Deux-Sèvres (79)
- Vienne (86)

Ses habitants sont les Picto-Charentais.

Elle a pour capitale régionale Poitiers qui est également la plus grande ville
et la plus grande agglomération urbaine de la région.

Située dans le Grand Sud-Ouest français, elle regroupe une population de
1 752 708 habitants en 2008 répartie sur 25 809 km21, soit une densité moyenne
de 68 hab./km2.

Elle est bordée par les régions Centre et Pays de la Loire au nord, Limousin à
l'est et enfin Aquitaine au sud. Sa façade ouest donne sur l'océan Atlantique.
