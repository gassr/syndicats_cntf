
.. index::
   pair: syndicat; ccs32

.. _ccs32:

=======================================================================================
Syndicat CNT ccs32 (Gers)
=======================================================================================

.. seealso::

   - :ref:`ud32_publique`
   - http://www.cnt-f.org/cnt31/spip.php?rubrique16

Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/index
