
.. index::
   pair: syndicat; usi32

.. _usi32:

=======================================================================================
Syndicat CNT USI32 (Gers)
=======================================================================================

.. seealso::

   - :ref:`ud32_publique`
   - http://www.cnt-f.org/cnt31/spip.php?rubrique16

Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/index
