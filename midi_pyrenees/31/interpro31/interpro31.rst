
.. index::
   pair: syndicat; interpro31



.. _interpro_31_pub:
.. _interpro31:

=======================================================================================
Syndicat CNT interpro31 (Toulouse)
=======================================================================================

.. seealso::

   - :ref:`ud31_publique`
   - http://www.cnt-f.org/cnt31/spip.php?rubrique16




Description
============

Le syndicat INTERPRO réunit tous les chômeurs, précaires et salariéEs qui sont
seulEs ou pas suffisamment nombreux dans leur secteur d’activité pour créer un
syndicat. Ainsi, il y a au syndicat des travailleurs du bâtiment , mais aussi
des précaires du secteur communication, culture, spectacle.

Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres
