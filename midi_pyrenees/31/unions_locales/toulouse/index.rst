
.. index::
   pair: Union Locale; Toulouse
   ! Toulouse


.. _ul_Toulouse_pub:

=======================================================================================
Union Locale des syndicats CNT de TOULOUSE (UL31)
=======================================================================================

.. seealso::

   - :ref:`ud33_publique`
   - http://www.cnt-f.org/cnt31/



::

	18, avenue de la Gloire
	31500 Toulouse
	Permanences tous les mardis et mercredis 18h00 à 20h00
	Tel : 09 52 58 35 90
	Mail :

	- Union locale Toulouse : cnt.31@cnt-f.org
	- Syndicat Interpro : interpro.31@cnt-f.org
	- Syndicat Santé/Social/Education : sse.31@cnt-f.org

Le local est également ouvert tous les vendredis de 19h00 à 22h00 et les
samedis de 15h00 à 19h00.
