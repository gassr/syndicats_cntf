
.. index::
   pair: syndicat; SSE31



.. _sse31_pub:
.. _sse31:

=======================================================================================
Syndicat SSE31 (Toulouse)
=======================================================================================

.. seealso::

   - :ref:`ud31_publique`
   - http://www.cnt-f.org/cnt31/spip.php?rubrique16


Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres
