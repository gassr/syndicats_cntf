
.. index::
   pair: syndicats; CNT Midi-Pyrénées
   pair: Union Régionale ; Midi-Pyrénées
   ! Midi-Pyrénées


.. _union_regionale_midi_pyrenees:

=======================================================
Union Régionale Midi-Pyrénées
=======================================================

.. seealso::

   - http://www.cnt-f.org/spip.php?article25
   - https://fr.wikipedia.org/wiki/Midi_pyrenees


.. figure:: midi_pyrenees_region_locator_map.svg.png
   :align: center

   *Midi-Pyrénées*


:code région: 73


La région Midi-Pyrénées (Miègjorn-Pirenèus en occitan) est une région du
sud-ouest de la France de tradition occitane qui regroupe huit départements:

- Ariège (09)
- Aveyron (12)
- Haute-Garonne (31)
- Gers (32)
- Lot (46)
- Hautes-Pyrénées (65)
- Tarn (81)
- Tarn-et-Garonne (82)

C’est la deuxième plus grande région de France (derrière la Guyane) avec huit
départements et plus de 45 000 km².

Sa plus grande ville est Toulouse qui est également sa préfecture.


.. toctree::
   :maxdepth: 4

   09/09
   31/31
   32/32
