
.. index::
   pair: syndicats; CNT Auvergne
   pair: Union Régionale ; Auvergne
   ! Auvergne


.. _union_regionale_auvergne:

=======================================================
Union Régionale Auvergne
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Auvergne


.. figure:: auvergne.png

   *L'Auvergne*


:code région: 83

Elle compte 4 départements:

- Allier (03)
- Cantal (15)
- Haute-Loire (43)
- Puy-de-Dôme (63)
