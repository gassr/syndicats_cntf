
.. index::
   pair: syndicat; stp72


.. _stp72_pub:

=======================================
Syndicat stp72
=======================================

.. seealso:: :ref:`ud72_publique`


Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres
