
.. index::
   pair: syndicat; ste72


.. _ste72_pub:

=======================================
Syndicat STE72
=======================================

.. seealso:: :ref:`ud72_publique`


Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres
