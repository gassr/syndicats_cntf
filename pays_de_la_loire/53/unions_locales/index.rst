
.. index::
   pair: Unions locales; Mayenne (53)


.. _unions_locales_mayenne:

===========================================
Unions Locales de la  Mayenne (UD53)
===========================================


Syndicats CNT Mayenne(53) .

.. seealso::

   - http://www.cnt-f.org/spip.php?article22
   - :ref:`ud53_publique`


.. toctree::
   :maxdepth: 4

   laval/index
