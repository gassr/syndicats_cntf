
.. index::
   pair: syndicats; CNT Pays de la Loire
   pair: Union Régionale ; Pays de la Loire
   ! Pays de la Loire


.. _union_regionale_pays_de_la_loire:

=======================================================
Union Régionale Pays de la Loire
=======================================================

.. seealso::

   - http://www.cnt-f.org/spip.php?article34 (Bretagne)
   - http://fr.wikipedia.org/wiki/Pays_de_la_Loire
   - :ref:`union_regionale_bretagne`


.. figure:: pays_de_la_loire.png

   *La région Pays de la Loire*


:code région: 52


La région est la 5e de France en termes de population et de produit intérieur
brut.

Les habitants des Pays de la Loire sont les Ligériens et les Ligériennes.

Au 1er janvier 2008, la population des Pays de la Loire était de 3 510 170 habitants.

Les grandes concentrations de population s'organisent autour des sept grands
pôle urbains qui dépassent les 100 000 habitants : Nantes, Angers, Le Mans,
La Roche-sur-Yon, Cholet, Saint-Nazaire et Laval


La région compte 5 départements:

- Loire-Atlantique (44)
- Maine-et-Loire (49)
- Mayenne (53)
- Sarthe (72)
- Vendée (85)


Unions Départementales CNT Pays de la Loire
============================================


.. toctree::
   :maxdepth: 4

   44/44
   49/49
   53/53
   72/72
   85/85
