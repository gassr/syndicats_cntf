
.. index::
   pair: Effondrement; 2018-11-17

.. _effondrement_2018_11_17:

====================================================
Le 17 NOVEMBRE 2018: Tapez là où ça leur fera mal !
====================================================

Face à la hausse des prix du carburant, un mouvement spontané de colère s’est
développé.

Les classes populaires et moyennes sont attaquées une fois de plus au
portefeuille et ont encore plus de mal à boucler les fins de mois.

Il n’a pas fallu bien longtemps pour que les populistes de tout poil en fassent
leur beurre. De surenchères en surenchères, l’extrême-droite et la droite
extrême cherchent à tout prix à récupérer ce mouvement.
Toutefois, chacun d’entre nous doit prendre conscience que cette hausse du
carburant n’est qu’un élément parmi d’autres qui nous montre que notre mode
de vie est condamné à disparaître.

La cause du mal n’est jamais nommée réellement, le responsable c’est le
capitalisme. Depuis le Capital, on savait le capitalisme suicidaire, mais là
il a allumé le gaz et va faire sauter tout l’immeuble, et nous avec.
Les capitalistes ont mis en place dans les pays riches une société de
consommation à outrance où l’individu.e n’existe et n’a de sens que dans ce
qu’il possède (voiture / TV / le dernier Apple coûte un SMIC mensuel !)

Nous le savons que ce mode de vie est en train de détruire notre planète car
il n’y aura jamais assez de ressources et le pétrole va être de plus en plus
rare donc de plus en plus cher !
Et à qui profite tout ça ? Et bien vous le savez ! Les capitalistes, ces 1%
qui possèdent autant de richesse les 99% d’autres.
L’homme le plus riche de France gagne en 1h ce que nous mettons 1 an à gagner !
Les 56 personnes les plus riches du monde possèdent autant de richesses que
les 3.5 MILLIARDS d’habitants les plus pauvres.

Et pour fabriquer tout ça, les capitalistes exploitent les peuples des pays
pauvres pour continuer à s’enrichir. Ils sont loin de nos regards mais nous
ne pouvons consommer que s’ils sont exploités !
A chaque Smartphone acheté, c’est un.e enfant.e du Nord Kivu (Congo) qui est
mis.e en esclavage. Les capitalistes ont mis en place de longue date
l’obsolescence programmée car oui si l’on ne consomme plus, leurs profits
s’effondreront !

Face à la dégradation de nos environnements, la disparition des espèces
animales, aux inégalités sociales, à la situation les migrant.e.s, des luttes
et des combats naissent chaque jour partout dans le monde et ils ont le même
ennemi : le capitalisme comme modèle d’organisation sociale.

Face à ces contestations toujours plus nombreuses, les capitalistes et les
gouvernements serrent la vis, criminalisent les mouvements sociaux, frappent
et tuent !
Les capitalistes et les gouvernants tentent de détruire chaque jour un peu
plus toutes les solidarités et les protections sociales.
Le projet des capitalistes est celui-ci : tout pomper jusqu’à la dernière
goutte, jusqu’à la dernière ressource, jusqu’aux derniers dollars, frapper
sur les têtes qui se relèveraient et après eux, le déluge...

Si l’on souhaite sauver ce qui peut l’être et permettre à nos enfants et petits
enfants de vivre tout simplement, nous devons changer radicalement de mode de
vie et détruire le modèle capitaliste.

Nous revendiquons:

- une société solidaire, sociale et libertaire.
- Une société basée sur une production coopérative, on produit ce dont nous
  avons besoin,
- des écoles émancipatrices, des transports en communs pour tou.t.es grâce à un
  service public de qualité.
- Une société où l’on pourrait travailler et consommer là où on vivrait.
- Une société où personne ne pourrait avoir le superflu tant que tout le monde
  n’a pas le nécessaire.

Pour le 17 novembre 2018, plutôt que de cramer de l’essence pour bloquer une
route nous invitons à « taper » là où ça leur fera mal : dans le portefeuille
des capitalistes, le 17 novembre boycottez les commerces, refusez de prendre
votre véhicule et faites leur perdre de l’argent !

Virez les populistes de vos actions et créons collectivement un mouvement
social plus large avec la construction d’une grève générale nationale et
illimitée.

Après le 17 novembre ? Rendez-vous dans la rue le 23 novembre 2018 pour une
marche pour le climat et contre la société de consommation, contre le
Black Friday !
