.. index::
   pair: Syndicat; Interco 49
   pair: Syndicat; SINR49
   pair: Interco; 49


.. _ste49:
.. _ste49_pub:

=======================================================================
Syndicat de l'éducation STE49
=======================================================================


.. seealso::

   - `http://www.cnt-f.org/_cnt-ste-49_.html`
   - :ref:`ud49_publique`




Présentation
============


Adresse
========



Site web
==========================================





Motions congrès STE49
======================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres
