
.. index::
   pair: Syndicat; Interco 44
   pair: Syndicat; SINR44
   pair: Interco; 44


.. _interco44_pub:
.. _interco44:
.. _sinr44:

============================================================================
Syndicat **intercorporatif de Nantes et et sa région Interco 44** (SINR44)
============================================================================


- http://www.cnt-f.org/_cnt-sinr-44\_.html
- :ref:`ud44_publique`


Présentation
============


Le Syndicat Intercorporatif de Nantes et sa Région (SINR ou interco 44)
est la forme actuelle de la CNT à Nantes. Il permet de regrouper tous
ceux qui dans la région nantaise souhaitent participer à la CNT.

Adresse
========

    C/o B17
    17 rue Paul BELLAMY
    44000 NANTES
    Tel : 0645364444


:Adresse courriel: interco44@cnt-f.org

Permanence tous les lundis 19h00-20h30.


Site web (http://www.cnt-f.org/ulnantes/)
==========================================

.. seealso::

   - http://ulnantes.cnt-f.org
   - http://www.cnt-f.org/ulnantes/




Motions congrès SINR44
======================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres
