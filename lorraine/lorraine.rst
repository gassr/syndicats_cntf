
.. index::
   pair: syndicats; CNT Lorraine
   pair: Union Régionale ; Lorraine
   ! Lorraine


.. _union_regionale_lorraine:

=======================================================
Union Régionale Lorraine
=======================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Lorraine
   - http://www.cnt-f.org/spip.php?article26


.. figure:: Lorraine_region_locator_map.svg.png
   :align: center

   *La Lorraine*


:code région: 41


La Lorraine (Louréne en lorrain roman et Lothringe(n) en francique lorrain et
francique rhénan) est une région au nord-est de la France dont le nom est
hérité du duché du même nom.


La Lorraine regroupe quatre départements:

- Meurthe-et-Moselle (54)
- Meuse (55)
- Moselle (57)
- Vosges (88)

et compte 2 337 communes.  La préfecture de région est Metz.

Ses habitants sont appelés les Lorrains et étaient au nombre de 2 346 361 au
1er janvier 2008.

La superficie de la Lorraine est de 23 547 km², chaque département ayant
approximativement la même superficie (environ 6 000 km²).

Son point culminant est le Hohneck à 1 364 m ; son point le plus bas est de
115 m au niveau de la rivière Saulx.


.. toctree::
   :maxdepth: 4

   syndicats_regionaux/syndicats_regionaux



.. toctree::
   :maxdepth: 4

   57/57

Adresse
=======

:Adresse courriel: ur.lorraine@cnt-f.org

::

    5 Place des Charrons
    57 000 Metz
