.. index::
   pair: syndicat; etpics57


.. _etpics57:
.. _etpics_57_pub:

=====================================================================================================================================
ETPICS57 pour syndicat des Employés, des Travailleurs et des Précaires des Industries, du Commerce et des Services **(en sommeil)**
=====================================================================================================================================

- http://www.cnt-f.org/ud57/spip.php?rubrique3
- :ref:`ud57_publique`
- etpics57nordsud@cnt-f.org

Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres
