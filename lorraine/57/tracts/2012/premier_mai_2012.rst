
.. index::
   pair: Premier mai; 2012 (Moselle, 57)

.. _tract_premier_mai_2012_ud57:

==================================================
1er Mai : le syndicalisme n'est pas qu'une fête !
==================================================

A l'heure où les élites dirigeantes se disputent le pouvoir, les populations et
les travailleurs ne cessent de souffrir ! Précarité de l'emploi, destruction
des services publics, remise en cause des acquis sociaux (retraites, sécurité
sociale, code du travail...) : nous ne faisons que subir tandis qu'eux
organisent notre misère.

Alors que les uns défendent l'austérité, pendant que les autres veulent relancer
la croissance, tous brandissent le même prétexte pour demander aux travailleurs
de se serrer la ceinture pendant que les patrons augmentent leurs bénéfices :
la crise économique !

Nous le voyons bien, les intérêts que défendent l'État et le patronat ne sont
pas ceux des classes  populaires. En Grèce, en Espagne, dans les Antilles,
au Maghreb et partout ailleurs ce sont les mêmes Etat et les mêmes capitalistes
qui nous exploitent.

Pour assoir cette domination, ils cherchent chaque jours à nous diviser.

En opposant chômeurs et salariés, travailleurs français et étrangers, hommes et
femmes, en jouant avec la peur de l'autre, en stigmatisant l'Islam
(comme hier le Judaïsme) et les banlieues, les homosexuels, les roms et toutes
les minorités, les politiciens de tous bords cherchent à nous faire oublier
que nos intérêts de classe sont communs.

C'est ainsi qu'ils isolent les individus et leurs luttes et se protègent de
toute contestation sociale. Ne l'oublions pas, c'est en profitant d'un contexte
si nauséabond que l'extrême droite et le fascisme ont écrit les pages les plus
sombres de notre Histoire.

En ce contexte électoral la CNT ne donnera donc aucune consigne à qui que ce
soit. Notre réponse à la crise économique et à la domination qu'ils nous
imposent n'existe que sur le terrain social !

C'est par un syndicalisme de combat, qui organise les classes populaires et
tisse des liens de solidarité entre nous, toutes et tous, travailleurs-euses
ou voisin-e-s, que nous entendons créer le rapport de force face au patronat
et à l'Etat.

Pour porter le mouvement social et notre projet de société, seule l'autogestion
des luttes et donc l'autogestion des syndicats  paient.

C'est aux individus en lutte de décider et de mettre en place les actions
qu'ils jugent nécessaires, sans déléguer leur pouvoir à quelque hiérarchie
syndicale ou parti politicien que ce soit.

L'anarchosyndicalisme de la CNT, qui refuse les permanents syndicaux et qui a
fait le choix de l'indépendance financière notamment pour ses locaux, réaffirme
que c'est par le biais des assemblées générales et de l'action directe que nous
obtiendrons gain de cause, que nous nous émanciperons.

C'est également en développant une contre culture populaire, en se formant
politiquement et syndicalement, en créant des alternatives économiques comme des
coopératives de productions ou de distributions, en renforçant la solidarité
internationale, que nous créerons le point de rupture avec le capitalisme et
arriverons à la révolution sociale et libertaire !

En créant des collectifs de lutte conter la précarité (tels que dans
l'Education Nationale où la CNT a été le seul syndicat à organiser les
précaires en contrat aidés) que nous permettons aux individus de conquerir
leurs droits et de retrouver leur dignité !

C'est en créant des sections syndicales CNT, que nous concrétisons cette
stratégie : Ikéa, AGCO, Tenex... sont autant d'expériences récentes qui nous
montrent que notre stratégie paie !

Aujourd'hui en Grèce la situation est presque insurrectionnelle.

En Espagne le 29 Mars dernier, un appel historique des syndicats de lutte
(CNT, CGT-E, Solidaridad Obrera) déclenchait une première journée de grève
générale dans tout le pays...

A Francfort, le 31 Mars, une grande manifestation à l'appel des
anarchosyndicalistes et syndicalistes révolutionnaires européens réunissait
plus de 6000 personnes devant la BCE...

Partout en Europe et dans le monde, les luttes des travailleurs s'intensifient
et renforcent notre détermination !

C'est pourquoi la CNT souhaite faire de ce premier Mai un véritable fête du
syndicalisme de lutte et de la solidarité internationale, mais également le
point de départ d'une véritable mobilisation des travailleurs et des
travailleuses vers la grève générale.

C'est aussi pour développer la solidarité internationale qu'elle vous invite à
rencontrer, dès le 2 Mai dans ces locaux, place des Charrons, nos camarades
grecs de l'ESE, actuellement en tournée dans toute la France pour partager
leurs expériences de lutte et de resistance.
