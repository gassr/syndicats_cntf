

.. _festival_ud57_2012:

==============================================
Festival Un Autre Futur (5 au 8 décembre 2012)
==============================================

.. seealso::

   - http://www.cnt-f.org/ud57/news.php?id_news=119




Introduction
============

Il y a un peu plus de 5 ans, la CNT lançait un pari fou, implanter un festival
anarcho-syndicaliste dans la durée, en faire un point d'appui pour le
développement de l'autogestion.

Force est de constater que malgré toutes les difficultés, tant financières que
politiques, nous y sommes parvenus tout en maintenant notre totale indépendance
financière. La CNT ne touche aucune subvention ni de l'Etat, ni des Patrons.

C'est une fierté.

Certain-e-s pensaient que le changement de majorité amènerait une société plus
juste. Les cadeaux fiscaux au patronat, la hausse de la TVA et de la CSG, les
milliers de licenciements, la chasse aux roms, la répression du mouvement social
telle que pratiquée à Notre Dame des Landes sont l'amère réalité issue des urnes.

Le Parti Socialiste et l'UMP sont les deux facettes de la même médaille.

Quant à la fille de son père, il est temps de la remettre en place.
Qui la finance si ce ne sont les grands patrons, financiers et industriels ?

Et toi travailleur, qui sont tes ennemis si ce ne sont ces derniers, ceux qui
te licencient et délocalisent les emplois ?

Tout en détruisant la planète dans leur besoin effrené de richesse...que tu
produis !

Face à l'exploitation capitaliste, au racisme, au chômage, à la guerre, à la
misère et au sexisme nous proposons la Solidarité internationale des
travailleurs et l'égalité économique, politique et sociale.

C'est en ce sens que nous proposons cet évènement, afin de débattre, d'échanger
et de construire. Construire les luttes qui nous permettrons de converger vers
la Grève Générale et de renverser la dictature capitaliste.

Cela en devient urgent et vital, il suffit pour s'en convaincre d'observer la
situation internationale ! Et puis, comme tous les ans, se retrouver ensemble
pour faire la fête ... d'ailleurs à ce sujet, le programme va encore une fois
être terrible.
