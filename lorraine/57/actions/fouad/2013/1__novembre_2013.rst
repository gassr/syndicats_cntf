


.. _fouad_1_novembre_2013:

==========================================================================
Suites de la condamnation du camarade Fouad ETPICS 57 (1er novembre 2013)
==========================================================================


::

	Sujet: 	[Liste-syndicats] Suites de la condamnation du camarade Fouad ETPICS 57
	Date : 	Fri, 1 Nov 2013 19:22:28 +0100
	De : 	"CNT-secrétariat confédéral adjoint" <administration@cnt-f.org>
	Pour : 	liste-syndicats@bc.cnt-fr.org


Cher-es camarades,

Comme vous le savez, notre camarade Fouad militant de la CNT ETPICS 57 et
secrétaire confédéral chargé des relations médias a été condamné par le
tribunal correctionnel de Metz à une amende de 40000 euros pour le blocage
de train en gare de Metz lors du mouvement anti-CPE de 2006.

L'audience en appel a eu lieu le 11 octobre dernier. La SNCF plaidait
l'irrecevabilité de l'appel au motif que celui-ci devait intervenir dans
les 10 jours à compter du prononcé du jugement et non à partir de sa
notification.

Les magistrats ont été dans l'obligation de reporter cette audience dans
le sens où l'avocat de notre camarade a déposé la veille une QPC (question
prioritaire de constitutionnalité) à ce sujet, estimant que l'accès au
juge (droit constitutionnel) serait entravé par les délais prescrits dans
le code pénal. En effet, l'avocat de notre camarade estimant qu'il y a une
incohérence en l'état, car comment une personne peut-elle faire appel
d'une décision sans avoir eu connaissance du jugement ?

Cet aspect technique sera donc traité le 08 novembre prochain soit trois
jours après le procès des 5 camarades de la CGT de Roanne. Cette nouvelle
audience, conditionnera de fait le recours en appel, elle sera donc très
importante dans la poursuite du recours de défense engagé par notre
camarade.

Ce ne sera qu'à l'issue de cette QPC le 08 novembre, que nous aurions une
nouvelle date d'audience pour l'appel. Des actions en fonction, seront
donc à organiser.

Nous ne manquerons pas de vous tenir informé des suites.


Solidarité avec notre camarade !

Recevez nos salutations AS & SR
Pour le Bureau confédéral, Niko
administration@cnt-f.org
