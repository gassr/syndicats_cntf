

.. _motions_etpics57_2021:
.. _motions_sest_lorraine_2021:

======================================================
Motions, amendements congres SEST Lorraine 2021
======================================================

Amendements
===============

- :ref:`amendement_motion_17_2021_sest_lorraine`
- :ref:`amendement_motion_2_2021_sest_lorraine`
- :ref:`amendement_motion_9_2021_sest_lorraine`
- :ref:`amendement_motion_22_2021_sest_lorraine`
- :ref:`amendement_motion_24_2021_sest_lorraine`
