.. index::
   pair: syndicat; SEST Lorraine
   pair: Lorraine; SEST
   ! 57


.. _sest_lorraine:

========================================================================================================================
Syndicat CNT des travailleurs-ses de la Santé, de l'Education, du Social et de la Territoriale (**CNT SEST Lorraine**)
========================================================================================================================

- http://www.cnt-f.org/_cnt-sante-social-et-ct-lorraine\_.html
- :ref:`ud57_publique`


::

   CNT SEST Lorraine
   BP 30 343
   57283 Maizieres-les-Metz

   sestlorraine@cnt-f.org


.. _etpics57_to_sipmcs:

Historique du changement de nom etpics57 => fusion de SSCT Lorraine et STE 57
===================================================================================


Annonce février 2018
---------------------

Les adhérents du syndicat STE 57, en sommeil depuis plus d'un an, ont
proposé au SSCT Lorraine d'intégrer le champ de syndicalisation de
l'Education dans leurs statuts afin de fusionner les deux syndicats.

Notre syndicat a décidé de valider la fusion car le champ de syndicalisation
de l'Education est proche de ceux que le SSCT Lorraine couvre actuellement.
La fusion permet donc de rassembler dans le même syndicat des travailleuses-rs
exerçant sur un même lieu de travail et/ou avec un même public alors
que cela n'était pas possible auparavant.

Le SSCT Lorraine fusionne donc officiellement avec le STE 57 et prend
désormais le nom de syndicat CNT des travailleurs-ses de la Santé, de
l'Education, du Social et de la Territoriale (CNT SEST Lorraine).

Nous sommes actuellement en train de changer les statuts du SSCT Lorraine
pour étendre les champs de syndicalisation à l'Education afin de ne pas
perdre notre ancienneté dans la fusion.

Nous communiquerons les informations au :term:`BC` pour que celui-ci
puisse valider cette fusion afin de nous permettre d'avoir de nouveaux
outils de fonctionnement (mail en cnt-f, liste mails...)

Le nouveau syndicat sera affilié aux 3 fédérations CNT qui couvrent ses
champs de syndicalisation à savoir:

- l'Education,
- le Santé-Social
- et la Fonction Publique Territoriale.


Activités
=========

.. toctree::
   :maxdepth: 4

   activites/index


Motions congrès
================

.. toctree::
   :maxdepth: 4


   motions_congres/motions_congres
