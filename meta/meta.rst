.. index::
   ! meta-infos

.. _syndicats_meta_infos:

=====================
Meta infos
=====================

- https://cnt-f.gitlab.io/meta/

Gitlab project
================

.. seealso::

   - https://gitlab.com/cnt-f/syndicats


Issues
--------

.. seealso::

   - https://gitlab.com/cnt-f/syndicats/-/boards


pipelines
------------

.. seealso::

   - https://gitlab.com/cnt-f/syndicats/-/pipelines


pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
